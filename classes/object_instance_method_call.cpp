// object instance & method call

#include <iostream>

using namespace std;

class Rectangle {
private:
  int width;
  int height;
public:
  Rectangle() {}
  Rectangle(int a, int b):width(a), height(b) {}
  ~Rectangle() {}

  void surfaceArea();

};

void Rectangle::surfaceArea() {
  cout <<  width * height << endl;
}

int main() {
  Rectangle(3, 4).surfaceArea(); // object instance & method call
  Rectangle(5, 5).surfaceArea(); // s/a

  return 0;
}
