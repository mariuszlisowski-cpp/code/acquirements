#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <class T>
class LinkedList {
private:
    int m_count;
public:
    Node<T> * Head;
    Node<T> * Tail;

    LinkedList();
    Node<T> * get(int);

    void insertHead(T);
    void insertTail(T);
    void insert(int, T);

    void removeHead();
    void removeTail();
    void remove(int);

    int search(T);

    int count();
    void printList();
};

template <class T>
LinkedList<T>::LinkedList():m_count(0), Head(nullptr), Tail(nullptr) {}

template <class T>
Node<T> * LinkedList<T>::get(int index) {
    if (index < 0 || index > m_count)
        return nullptr;
    Node<T> * node = Head;
    for (int i = 0; i < index; ++i)
        node = node->Next;
    return node;
}

template <class T>
void LinkedList<T>::insertHead(T val) {
    Node<T> * node = new Node<T>(val);
    node->Next = Head;
    Head = node;
    if (m_count == 0)
        Tail = Head;
    m_count++;
}

template <class T>
void LinkedList<T>::insertTail(T val) {
    if (m_count == 0) {
        insertHead(val);
        return;
    }
    Node<T> * node = new Node<T>(val);
    Tail->Next = node;
    Tail = node;
    m_count++;
}

template <class T>
void LinkedList<T>::insert(int index, T val) {
    if (index < 0 || index > m_count)
        return;
    if (index == 0) {
        insertHead(val);
        return;
    }
    else if (index == m_count) {
        insertTail(val);
        return;
    }
    Node<T> * prevNode = Head;
    for (int i = 0; i < index - 1; ++i)
        prevNode = prevNode->Next;
    Node<T> * nextNode = prevNode->Next;
    Node<T> * node = new Node<T>(val);
    node->Next = nextNode;
    prevNode->Next = node;
    m_count++;
}

template <class T>
int LinkedList<T>::search(T val) {
    if (m_count == 0)
        return -1;
    int index = 0;
    Node<T> * node = Head;
    while (node->value != val) {
        index++;
        node = node->Next;
        if (node == NULL)
            return -1;
    }
    return index;
}

template <class T>
void LinkedList<T>::removeHead() {
    if (m_count == 0)
        return;
    Node<T> * node = Head;
    Head = Head->Next; // Head = node->Next; ????????????
    delete node;
    m_count--;
}

template <class T>
void LinkedList<T>::removeTail() {
    if (m_count == 0)
        return;
    if (m_count == 1) {
        removeHead();
        return;
    }
    Node<T> * prevNode = Head;
    Node<T> * node = Head->Next;
    while (node->Next != NULL) {
        prevNode = prevNode->Next;
        node = node->Next;
    }
    prevNode->Next = nullptr;
    Tail = prevNode;
    delete node;
    m_count--;
}

template <class T>
void LinkedList<T>::remove(int index) {
    if (m_count == 0)
        return;
    if (index < 0 || index >= m_count)
        return;
    if (index == 0) {
        removeHead();
        return;
    }
    else if (index == m_count - 1) {
        removeTail();
        return;
    }
    Node<T> * prevNode = Head;
    for (int i = 0; i < index - 1; ++i)
        prevNode = prevNode->Next;
    Node<T> * node = prevNode->Next;
    Node<T> * nextNode = node->Next;
    prevNode->Next = nextNode;
    delete node;
    m_count--;
}

template <class T>
int LinkedList<T>::count() {
    return m_count;
}

template <class T>
void LinkedList<T>::printList() {
    Node<T> * node = Head;
    while (node != NULL) {
        std::cout << node->value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}

#endif
