// stack
// by raisin

#include <iostream>
#include "node.hpp"
#include "stack.hpp"

using namespace std;


int main() {
    Stack stack;

    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);
    stack.pop();

    stack.showStack();

    cout << "Top: " << stack.top() << endl;

    return 0;
}
