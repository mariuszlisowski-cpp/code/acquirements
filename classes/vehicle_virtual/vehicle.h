// base class header

#pragma once

using std::string;

class Vehicle {
protected:
  string model;
  int enginePower;
public:
  // constructors
  Vehicle();
  Vehicle(const string, const int);

  // virtual destructor
  virtual ~Vehicle(); // must be virtual

  // virtual function
  virtual void display() const; // can be overriden in a derived class

  // pure virtual function
  virtual void drive() = 0; // must be overriden
};
