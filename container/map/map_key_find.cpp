// Map container key find

#include <vector>
#include <iostream>
#include <map>

using namespace std;


int main() {
  string s;
  vector<string> names;
  map<string, int> phonebook;

  int t, n;
  cout << "How many entries?" << endl << ": ";
  cin >> t;
  while (t--) {
    cout << "Enter a name followed by a phone number" << endl << ": ";
    cin >> s >> n;
    phonebook[s] = n;
  }

  cout << "How many searches?" << endl << ": ";
  cin >> t;
  while (t--) {
    cout << "A name to be found?" << endl << ": ";
    cin >> s;
    names.push_back(s);
  }

  vector<string>::const_iterator it;
  for (it = names.begin(); it != names.end(); it++) {
    if (phonebook.find(*it) != phonebook.end())
      cout << *it << "'s phone no: " << phonebook[*it] << endl;
    else
      cout << *it << " - not found" << endl;
  }

  return 0;
}
