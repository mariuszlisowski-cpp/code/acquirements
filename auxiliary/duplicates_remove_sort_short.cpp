// string duplicates

#include <iostream>
#include <algorithm>

bool areAllCharactersUnique(std::string str) {
    std::sort(str.begin(), str.end());
    return (std::unique(str.begin(), str.end()) == str.end());
}

int main() {
    std::string s {"01234567890"};
    if (areAllCharactersUnique(s))
        std::cout << "No duplicates" << std::endl;
    else
        std::cout << "Duplicates found!" << std::endl;

    return 0;
}
