#pragma once

template <class T>
class DoublyNode {
public:
    T value;
    DoublyNode<T> * Previous;
    DoublyNode<T> * Next;

    DoublyNode(T);
};

template <class T>
DoublyNode<T>::DoublyNode(T val):value(val), Previous(nullptr), Next(nullptr) {}
