// linked list find & delete

#include <iostream>

struct Node {
   int data;
   Node* next = NULL;
};

using namespace std;

Node* findNode(Node* const &, int);
int findAllNodes(Node* const &, int);
void insertNode(Node* &, int);
void deleteNode(Node* &);
void traverseList(Node* const &);

int main() {
   const int find = 9;
   Node* linkedList = NULL;

   for (int i = 1; i <= 9; i++)
      insertNode(linkedList, i);
   insertNode(linkedList, 9); // duplicate the last node

   traverseList(linkedList);

   // counting all occurences of data
   int occ = findAllNodes(linkedList, find);
   cout << "~ found " << occ << " occurence(s) of " << find << endl;

   // looking for the first occurence of data
   Node* found = findNode(linkedList, find);
   deleteNode(found);
   traverseList(linkedList);

   // counting all occurences of data
   occ = findAllNodes(linkedList, find);
   cout << "~ found " << occ << " occurence(s) of " << find << endl;

   return 0;
}

// count all data occurences in a list
int findAllNodes(Node* const &head, int d) {
   int counter = 0;
   Node* found;
   found = findNode(head, 9);
   while (found) {
      counter++;
      found = findNode(found->next, 9);
   }
   return counter;
}

// find the first data occurence in a list
Node* findNode(Node* const &head, int d) {
   if (head == NULL)
      return NULL;
   Node* temp = head;
   while (temp) {
      if (temp->data == d)
         return temp;
      temp = temp->next;
   }
   return NULL;
}

// insert a new node at tail
void insertNode(Node* &head, int d) {
   Node* newNode = new Node;
   newNode->data = d;
   // create the first node
   if (head == NULL)
      head = newNode;
   // create further nodes (at tail)
   else {
      // ...move from head
      Node* temp = head;
      // ...to tail
      while (temp->next != NULL)
         temp = temp->next; // the last node found
      // ...and insert at tail
      temp->next = newNode;
   }
}

// delete the node at the given pointer
void deleteNode(Node* &node) {
   Node* temp;
   // get the next node
   temp = node->next;
   // replace the current node with the next
   node->data = temp->data;
   node->next = temp->next;
   // deleting the next node
   delete temp;
}

// traverse the list from the head
void traverseList(Node* const &head) {
   // empty list
   if (head == NULL)
      return;
   // non empty
   Node* temp = head;
   cout << "(head) ";
   while (temp != NULL) {
      cout << temp->data << " ";
      temp = temp->next; // points to NULL
   }
   cout << "NULL (tail)" << endl;
}
