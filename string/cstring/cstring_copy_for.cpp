#include <iostream>

int main() {
    char carr1[] {"C++"};
    char cstr2[sizeof(carr1)] {};

    for (size_t i = 0; i < sizeof(carr1) - 1; ++i) {
        cstr2[i] = carr1[i];
    }

    std::cout << cstr2 << std::endl;
}
