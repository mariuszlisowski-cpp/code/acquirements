#include <iostream>

int main() {
    char cstr[] {"Programowanie w jezyku C++"};
    size_t count{};
    while (cstr[count] != '\0') {
        ++count;
    }
    std::cout << count << std::endl;
    std::cout << sizeof(cstr) - 1 << std::endl; // without '\0'
}
