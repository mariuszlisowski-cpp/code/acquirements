#pragma once

class Node {
public:
    int value;
    Node * Next;
    Node(int);
};

Node::Node(int val = 0):value(val), Next(nullptr) {}
