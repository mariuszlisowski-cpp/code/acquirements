// string to wstring conversion

#include <algorithm>
#include <codecvt>  // std::codecvt_utf8_utf16
#include <cwctype>  // ::iswspace, ::iswpunct
#include <iostream>
#include <locale>  // std::wstring_convert
#include <string>

// to wide string
std::wstring convertToWstring(const std::string& s) {
    std::wstring str =
        std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(s);
    return str;
}

// to string
std::string convertToString(const std::wstring& s) {
    std::string str =
        std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(s);
    return str;
}

int main() {
    std::string s{
        "Are we not pure? “No, sir!” Panama’s moody Noriega brags."
        "It is garbage!” Irony dooms a man—a prisoner up to new era."
    };

    // converting to wide string
    std::wstring ws = convertToWstring(s);

    // converting locale do display utf-8 in windows
    std::ios_base::sync_with_stdio(false);
    std::locale utf8(std::locale(), new std::codecvt_utf8_utf16<wchar_t>);
    std::wcout.imbue(utf8);

    // displaying
    std::wcout << ws << '\n';

    // converting to string
    std::string st = convertToString(ws);
    
    // displaying
    std::cout << st << '\n'; // default output

    return 0;
}