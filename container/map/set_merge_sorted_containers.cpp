// merge sorted containers

#include <iostream>
#include <set>
#include <algorithm>

int main()
{
    std::set<int> odd = { 1, 3, 5 };
    std::set<int> even = { 2, 4, 6 };

    std::set<int> merged;
    std::merge(odd.begin(), odd.end(),
                even.begin(), even.end(),
                std::inserter(merged, merged.begin()));

    for (auto const& e: merged) {
        std::cout << e << ' ';
    }

    return 0;
}
