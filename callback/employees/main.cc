#include "hr.h"
#include "hr_helper.h"
#include "employee.h"

int main() {
    std::vector<std::unique_ptr<Employee>> employees;
    employees.emplace_back(std::make_unique<Employee>("Lisa", 42));
    employees.emplace_back(std::make_unique<Employee>("Mary", 38));
    employees.emplace_back(std::make_unique<Employee>("Anne", 22));

    HR hr(std::move(employees));
    
    hr.processEmployee(HR_helper::showByName);
    hr.processEmployee(HR_helper::showByAge);

    return 0;
}
