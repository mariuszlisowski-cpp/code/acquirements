#include <iostream>
#include <string>

template <typename ... Args>
auto sumA(Args&& ... args) {
    return (args + ...);                           // (right) folding
}                                                  // (... + t) convertible here as adding

// saa
template <typename ... Args>
auto sumB(Args&& ... args) {
    return (... + args);                           // (left) folding
}                                                  // (t + ... ) convertible here as adding

int main() {
    auto the_sum{ sumA(2, 3, 5) };
    std::cout << the_sum << std::endl;

    std::string strA{ "hi " };
    std::string strB{ "you" };
    std::cout << sumA(strA, strB) << std::endl;

}
