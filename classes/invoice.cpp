/*
Invoice
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Faktura {
private:
  string data;
  float netto;
  float brutto;
  int podatek;
public:
  Faktura();
  Faktura(int);
  ~Faktura() {};

  string getData() { return data; }
  float getNetto() { return netto; }
  float getBrutto() { return brutto; }
  int getPodatek() { return podatek; }

  void setData(string aData) { data = aData; }
  void setNetto(int aNetto) { netto = aNetto; }
  void setBrutto(int aBrutto) { brutto = aBrutto; }
  void setPodatek(int aPodatek) { podatek = aPodatek; }

  float vat(float);
  float vat(float, int);
};

Faktura::Faktura():podatek(23) {}
Faktura::Faktura(int aPodatek):podatek(aPodatek) {}

float Faktura::vat(float aNetto) {
  netto = aNetto;
  return brutto = netto * podatek/100;
}

float Faktura::vat(float aNetto, int aPodatek) {
  netto = aNetto;
  podatek = aPodatek;
  return brutto = netto * podatek/100;
}

int main()
{
  Faktura fakturaA; // domyślny podatek 23%
  Faktura fakturaB(8); // przeciążenie konstruktora wartością int
  Faktura fakturaC(15); // j.w.

  fakturaA.setNetto(100);
  float net = fakturaA.getNetto();
  cout << "Kwota netto: " << net << " zł" << endl;
  cout << "Podatek A: " << fakturaA.vat(net) << " zł" << endl;

  cout << "Podatek B: " << fakturaB.vat(net) << " zł" << endl;
  cout << "Podatek C: " << fakturaC.vat(net) << " zł" << endl;

  fakturaA.setBrutto(fakturaA.vat(net) + fakturaA.getNetto());
  cout << "Kwota brutto faktury A: " << fakturaA.getBrutto() << endl;
  fakturaC.setBrutto(fakturaC.vat(net) + fakturaC.getNetto());
  cout << "Kwota brutto faktury C: " << fakturaC.getBrutto() << endl;

  cout << "Teraz podatek A: " << fakturaA.vat(100, 22) << " zł" << endl;
  cout << "Teraz podatek B: " << fakturaB.vat(100, 7) << " zł" << endl;
  cout << "Teraz podatek C: " << fakturaC.vat(100, 14) << " zł" << endl;

  fakturaB.setBrutto(fakturaB.vat(100, 7) + fakturaB.getNetto());
  cout << "Kwota brutto faktury B: " << fakturaB.getBrutto() << endl;

  string s("01/01/2020");
  fakturaA.setData(s);
  cout << "Faktura A jest z datą " << fakturaA.getData() << endl;

  return 0;
}
