#include <iostream>


int main() {
    int a = 20;
    int* b = &a;

    std::cout << b << std::endl;        // address
    std::cout << *b << std::endl;       // value
    
    
    std::cout << a << std::endl;        // value
    std::cout << *(&a) << std::endl;    // value
    
    return 0;
}
