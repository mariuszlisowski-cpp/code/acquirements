// Queue implementation (class)

#include <iostream>

using namespace std;

class Element {
public:
   int data = 0;
   Element* after = NULL;

   // constructor
   Element(int d){
      this->data = d;
      this->after = NULL;
   }
};

class Queue {
public:
   Element* first = NULL;
   Element* last = NULL;

   void add(int);
   void show();
   void rmv();
};

// adding to stack
void Queue::add(int d) {
   Element* n = new Element(d);

   if (this->first == NULL || this-> last == NULL) // first data added
   this->first = this->last = n;
   else {
      this->last->after = n;
      this->last = n;
   }
}

// display stack
void Queue::show() {
   int i = 0;
   Element* temp = this->first;
   while (temp != NULL) {
      cout << temp->data << "-";
      temp = temp->after;
      i++;
   }
   cout << "NULL [" << i << "]" << endl << endl;
   // delete temp; (?)
}

// rmv from stack
void Queue::rmv() {
   if (this->first != NULL) {
      Element* temp =  this->first;
      this->first = this->first->after;
      delete temp;
   }
}

int main() {

   Queue* myQ = new Queue;
   myQ->add(10);
   myQ->add(20);
   myQ->add(30);
   myQ->add(40);
   myQ->add(50);

   myQ->show(); // 10-20-30-40-50-NULL

   myQ->rmv(); // 10
   myQ->rmv(); // 20
   myQ->rmv(); // 30

   myQ->show(); // 40-50-NULL

   myQ->rmv(); // 40
   myQ->rmv(); // 50
   myQ->rmv(); // !
   myQ->rmv(); // !

   myQ->show(); // NULL

   return 0;
}
