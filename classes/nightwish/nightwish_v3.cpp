#include <iostream>

using namespace std;

class Album {
  private:
    int rating;
  public:
    string artist;
    string title;
    string release;
    int num;
    Album() {
      num = 0;
      artist = "Empty";
      title = "Empty";
      release = "Empty";
      rating = 0;
    };
    Album(int aNum,string aArtist, string aTitle, string aRelease, int aRating = 0) {
      num = aNum;
      artist = aArtist;
      title = aTitle;
      release = aRelease;
      rating = aRating;
    }
    ~Album() {};
    void setRating(int aRating) {
      if (aRating < 1 || aRating > 9) {
        cout << "Rating out of range. Please enter 1-9.";
        rating = 0;
      } else rating = aRating;
    }
    bool checkRating() {
      if (rating != 0) return true;
    }
    int getRating() {
      return rating;
    }
    void displayAlbum() {
        cout << "\nAlbum no:\t" << num << endl;
        cout << "Artist:\t\t" << artist << endl;
        cout << "Title:\t\t" << title << endl;
        cout << "Released:\t" << release << endl;
        cout << "Current rating:\t" << getRating() << endl;
    }
};

int main()
{
  int albums = 3;
  Album test(0, "Test", "Test", "Test");
  Album cd[6] = {
    Album(1, "Nightwish", "Angels Fall First", "1997", 8),
    Album(2, "Nightwish", "Ocean Born", "1998", 9),
    test
  };

  cout << "\nThe example of empty albums...";
  cd[3] = Album();
  cd[4] = Album(test);
  cd[5] = test;
  cd[2].displayAlbum();
  cd[3].displayAlbum();
  cd[4].displayAlbum();
  cd[5].displayAlbum();
  cout << "\n***********************************\n";

  for (int i=0; i<albums; i++)
    cd[i].displayAlbum();
  cout << "\n***********************************\n";

  int album;
  bool ok = true;
  do {
    cout << "\nPlease choose the album number to rate [1-" << albums << "]: ";
    cin >> album;
    if (album < 1 || album > albums) {
      cout << "Out of range. Please try again...";
      ok = false;
    }
    else break;
  }
  while (!ok);

  int rating;
  cout << "\n\nPlease rate the following album:";
  cd[album-1].displayAlbum();
  do {
    cout << "\nYour rating [1-9]: ";
    cin >> rating;
    cd[album-1].setRating(rating);
  } while(!cd[album-1].checkRating());
  cout << "\nNow your album is rated as follows: ";
  cd[album-1].displayAlbum();
  cout << "\n***********************************\n";

  return 0;
}
