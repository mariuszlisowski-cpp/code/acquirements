// derived class header

using std::string;

class Student : public Person {
public:
  Student();
  Student(string, int);
  virtual ~Student();

  // pure virtual function
  virtual void outputIdentity();

  // virtual function
  virtual void outputAge(int) const;
};
