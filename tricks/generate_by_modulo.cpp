#include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
#include <vector>

template<typename Container>
void print(Container c, typename Container::difference_type n = 10) {
    std::for_each(c.begin(), 
                  c.begin() + n,
                  [](const auto & el){
                      std::cout << std::setw(3) << el << " ";
                  });
}

int main() {
    std::vector<int> v(100);
    std::array<int, 3> ar{ -2, -1, 0 };

    std::generate(v.begin(),v.end(),
                  [i{ 0 }, &ar] () mutable {
                      return ar[(i++) % static_cast<int>(ar.size())];
                  });
    print(v);

    return 0;
}
