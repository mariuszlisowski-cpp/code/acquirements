#include <array>
#include <iostream>
#include <string>

class ipv4 {
public:
    ipv4();
    ipv4(const unsigned char a,
                   const unsigned char b,
                   const unsigned char c,
                   const unsigned char d);
    explicit constexpr ipv4(unsigned long);
    ipv4(const ipv4& other) noexcept; // copy constructor

    ipv4& operator=(const ipv4& other);
    friend std::ostream& operator<<(std::ostream& os, const ipv4& a);
    friend std::istream& operator>>(std::istream& is, ipv4& a);

    std::string to_string() const;
    constexpr unsigned long to_ulong() const noexcept;

private:
    std::array<unsigned char, 4> data_;

};
