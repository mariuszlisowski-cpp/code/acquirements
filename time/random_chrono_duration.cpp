#include <chrono>
#include <iostream>
#include <random>

template<typename Duration>
auto float_to_duration(const float time) {
    using float_seconds = std::chrono::duration<float>;

    return std::chrono::round<Duration>(float_seconds{time});
}

template<typename Time>
auto get_random_time(Time from, Time to) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<Time> dist(from, to);

    return dist(mt);
}

int main() {
    auto sleep_time = get_random_time<float>(0, 9.5);                                   // float
    {
    auto sleep_duration = float_to_duration<std::chrono::seconds>(sleep_time);          // std::chrono::seconds
    std::cout << sleep_duration << std::endl;
    }
    {
    auto sleep_duration = float_to_duration<std::chrono::milliseconds>(sleep_time);     // std::chrono::milliseconds
    std::cout << sleep_duration << std::endl;
    }

    return 0;
}
