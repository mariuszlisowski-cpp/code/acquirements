/*
Zapis i odczyt pliku
*/
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  /* zapis do pliku */
  ofstream zapis;
  zapis.open("plik.text");
  zapis << "ten tekst pojawi się w '    .text'";
  char enter('\n');
  zapis << enter;

  zapis.seekp(0);
  zapis.put('T'); // poprawi zdanie, by zaczynało się z wielkiej litery

  zapis.seekp(25); // pozycja do uzupełnienia
  char napis[]("plik");
  zapis.write(napis, 4); // nadpisuje 'napis' o długośi 4

  zapis.close();

  /* -------------------------------------------------------------------- */

  /* odczyt z pliku */
  ifstream odczyt;
  odczyt.open("plik.text");

  /* 1.
  odczyt z pliku linia po linii */
  string linia;
  while (!odczyt.eof())
  {
    getline(odczyt, linia);
    cout << linia << endl;
  }

  /* reset pliku przed ponownym użyciem */
  odczyt.clear(); // usunięcie flagi EOF (End Of File)
  odczyt.seekg(0); // wskazanie na początek pliku
  cout << ": pozycja pliku: " << odczyt.tellg() << endl;

  /* 2.
  odczyt z pliku znak po znaku */
  char znak;
  while (!odczyt.eof())
  {
    odczyt.get(znak);
    cout << znak;
  }

  /* długość pliku */
  odczyt.clear();
  odczyt.seekg(0, odczyt.end); // ustaw na koniec pliku
  int len = odczyt.tellg(); // zapamiętana długość pliku
  cout << ": liczba znaków w pliku: " << len << endl;

  /* 3.
  odczyt z pliku do tablicy znaków */
  odczyt.clear();
  odczyt.seekg(0);
  char blok[len + 1]; // tablica o długości pliku
  odczyt.read(blok, len); // odczytaj plik o dł. 'len' (do tablicy 'blok')
  cout << blok << endl;

  odczyt.close();

  return 0;
}
