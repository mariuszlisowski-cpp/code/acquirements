// find_if custom
// ~ implementation as it is in an alghoritm library
// #find_if

#include <iostream>
#include <vector>

using namespace std;

template<class InputIt, class UnaryPredicate>
constexpr InputIt find_if(InputIt first, InputIt last, UnaryPredicate p)
{
    for (; first != last; ++first) {
        if (p(*first)) {
            cout << "!" << endl;
            return first;
        }
    }
    return last;
}

int main() {

    vector<int> vec{1, 2, 4, 5, 6, 7, 8};
    vector<int>::iterator it = find_if(vec.begin(), vec.end(),
                                [](const int & val){
                                    if (val % 3 == 0)
                                        return true;
                                    return false;
                               });

    if (it != vec.end())
        cout << "Multiple of 3 found : " << *it << endl;
    else
        cout << "Multiple of 3 not found!" << endl;

    return 0;
}
