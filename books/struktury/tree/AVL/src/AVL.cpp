#include <algorithm>
#include "../include/AVL.hpp"

AVL::AVL():root(nullptr) {}

// HEIGHT **************************************
int AVL::GetHeight(BSTNode * node) {
    return node ? node->Height : -1;
}
// ROTATE **************************************
BSTNode * AVL::RotateLeft(BSTNode * node) {
    BSTNode * balancedNode = node->Right;
    balancedNode->Parent = node->Parent;
    node->Parent = balancedNode;
    node->Right = balancedNode->Left;
    if (balancedNode->Left)
        balancedNode->Left->Parent = node;
    balancedNode->Left = node;
    node->Height = std::max(
        GetHeight(node->Left),
        GetHeight(node->Right)) + 1;
    balancedNode->Height = std::max(
        GetHeight(balancedNode->Left),
        GetHeight(balancedNode->Right)) + 1;
    return balancedNode;
}
BSTNode * AVL::RotateRight(BSTNode * node) {
    BSTNode * balancedNode = node->Right;
    balancedNode->Parent = node->Parent;
    node->Parent = balancedNode;
    node->Left = balancedNode->Right;
    if (balancedNode->Right)
        balancedNode->Right->Parent = node;
    balancedNode->Right = node;
    node->Height = std::max(
        GetHeight(node->Left),
        GetHeight(node->Right)) + 1;
    balancedNode->Height = std::max(
        GetHeight(balancedNode->Left),
        GetHeight(balancedNode->Right)) + 1;
    return balancedNode;
}
// INSERT **************************************
void AVL::Insert(int key) {
    root = Insert(root, key);
}
BSTNode * AVL::Insert(BSTNode * node, int key) {
    if (!node) {
        node = new BSTNode;
        node->Key = key;
        node->Left = nullptr;
        node->Right = nullptr;
        node->Parent = nullptr;
        node->Height = 0;
    }
    else if (node->Key < key) {
        node->Right = Insert(node->Right, key);
        node->Right->Parent = node;
    }
    else {
        node->Left = Insert(node->Left, key);
        node->Left->Parent = node;
    }

    int balance = GetHeight(node->Left) - GetHeight(node->Right);
    if (balance == 2) {
        int balance2 = 
            GetHeight(node->Left->Left) -
            GetHeight(node->Left->Right);
        if (balance2 == 1)
            node = RotateRight(node);
        else {
            node->Left = RotateRight(node->Left);
            node = RotateRight(node);
        }
    }
    else if (balance == -2) {
        int balance2 =
            GetHeight(node->Right->Left) -
            GetHeight(node->Right->Right);
        if (balance2 == -1)
            node = RotateLeft(node);
        else {
            node->Right = RotateRight(node->Right);
            node = RotateLeft(node);
        }
    }
    node->Height = std::max(
        GetHeight(node->Left),
        GetHeight(node->Right)) + 1;
    return node;
}
// REMOVE **************************************
void AVL::Remove(int key) {
    root = Remove(root, key);	
}
BSTNode * AVL::Remove(BSTNode * node, int key) {
    if (!node)
        return nullptr;
    if (node->Key == key) {
        if (!node->Left && !node->Right)
            node = nullptr;
        else if (!node->Left && node->Right) {
            node->Right->Parent = node->Parent;
            node = node->Right;
        }
        else if (node->Left && !node->Right) {
            node->Left->Parent = node->Parent;
            node = node->Left;
        }
        else {
            int successorKey = Successor(key);
            node->Key = successorKey;
            node->Right = Remove(node->Right, successorKey);
        }
    }
    else if (node->Key < key)
        node->Right = Remove(node->Right, key);
    else
        node->Left = Remove(node->Left, key);

    if (node) {
        int balance = GetHeight(node->Left) - GetHeight(node->Right);
        if (balance == 2) {
            int balance2 =
                GetHeight(node->Left->Left) -
                GetHeight(node->Left->Right);
            if (balance2 == 1)
                node = RotateRight(node);
            else {
                node->Left = RotateLeft(node->Left);
                node = RotateRight(node);
            }
        }
        else if (balance == -2) {
            int balance2 =
                GetHeight(node->Right->Left) -
                GetHeight(node->Right->Right);
            if (balance == -1)
                node = RotateLeft(node);
            else {
                node->Right = RotateRight(node->Right);
                node = RotateLeft(node);
            }
        }
        node->Height = std::max (
            GetHeight(node->Left),
            GetHeight(node->Right)) + 1;
    }
    return node;
}