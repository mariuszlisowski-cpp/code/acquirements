// Stream output manipulators

#include <iostream>
#include <iomanip>

using namespace std;

void separator();

int main() {
  // setting field width
  const char * message = "wibble";
  cout << "[" << setw(10) << message << "]" << endl;
  cout << "[" << setw(10) << message << "]" << endl
       << "[" << message << "]" << endl; // applies only to first message
  separator();

  // output formatting
  cout << left << setw(10) << "hello" << "world" << endl;
  cout << right << setw(10) << "hello" << "world" << endl;
  cout << internal << setw(10) << -123.45 << endl;
  separator();

  // justification
  double pi = 3.14159265358979;
  cout << fixed << pi << endl;
  cout << scientific << pi << endl;
  cout.unsetf(ios::fixed | ios::scientific); // back to general formatting
  separator();

  // floating point format
  int x = 1, y = 2, z = 3;
  cout << showpos; // show positive sign
  cout << x << " " << y << " " << z << endl;
  cout << noshowpos;
  cout << x << " " << y << " " << z << endl;
  separator();
  cout << showbase; // shows 0 for oct and 0x for hex
  cout << 64 << endl;
  cout << oct << 64 << endl;
  cout << hex << 64 << endl;
  cout << noshowbase;
  separator();
  cout << hex;
  cout << 123456789 << endl;
  cout << uppercase << 123456789 << endl; // hex letters uppercase
  cout << nouppercase;
  cout << dec;
  separator();

  // precision
  double l = 42.0;
  double d = 23.45663332;
  cout << setprecision(3);
  cout << l << endl;
  cout << d << endl;
  cout << pi << endl;
  separator();
  cout << fixed; // changes precision behaviour
  cout << l << endl;
  cout << d << endl;
  cout << pi << endl;
  separator();

  return 0;
}

void separator() {
  cout << "---------------------" << endl;
}
