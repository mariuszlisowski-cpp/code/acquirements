// Project: Radix_Sort.cbp
// File   : Queue.h
#ifndef QUEUE_H
#define QUEUE_H

#include "Node.h"

template <typename T>
class Queue
{
private:
    int m_count;
    Node<T> * m_front;
    Node<T> * m_back;

public:
    Queue();
    bool IsEmpty();
    T Front();
    void Enqueue(T val);
    void Dequeue();
};

template <typename T>
Queue<T>::Queue() : m_count(0), m_front(nullptr), m_back(nullptr) {}

template <typename T>
bool Queue<T>::IsEmpty()
{
    // Zwraca true, je�li wyst�puje przynajmniej jeden element; w przeciwnym przypadku zwraca false.
    return m_count <= 0;
}

template <typename T>
T Queue<T>::Front()
{
    // Zwraca warto�� w�z�a m_front.
    return m_front->Value;
}

template <typename T>
void Queue<T>::Enqueue(T val)
{
    // Tworzy nowy w�ze�.
    Node<T> * node = new Node<T>(val);

    if(m_count == 0)
    {
        // Je�li kolejka jest pusta, nowy w�ze� definiowany jest jako m_front i m_back.
        node->Next = NULL;
        m_front = node;
        m_back = m_front;
    }
    else
    {
        // Je�li w kolejce znajduje si� co najmniej jeden element, bie��cy element m_back przestaje by� elementem Back,
        // wi�c wska�nik Next bie��cego m_back wskazuje nowy w�ze�.
        m_back->Next = node;

        // Nowy w�ze� staje si� tylnym elementem.
        m_back = node;
    }

    // Dodaje element.
    m_count++;
}

template <typename T>
void Queue<T>::Dequeue()
{
    // Nic nie robi, je�li lista jest pusta.
    if(m_count == 0)
        return;

    // Zapisuje bie��cy prz�d w nowym w�le.
    Node<T> * node = m_front;

    // Kieruje wska�nik Front do elementu nast�puj�cego po bie��cym.
    m_front = m_front->Next;

    // Mo�na bezpiecznie usun�� pierwszy element.
    delete node;

    // Usuwa element.
    m_count--;
}

#endif // QUEUE_H
