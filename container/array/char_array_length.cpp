/*
Char array length calculation
*/

#include <iostream>

using std::cout;
using std::endl;

size_t strlen(const char*);

int main() {
  char c[] = "example char array";
  cout << strlen(c) << endl;;
  return 0;
}

// calculates string length
size_t strlen(const char* start) {
   const char* end = start;
   while (*++end != 0);
   return end - start;
}
