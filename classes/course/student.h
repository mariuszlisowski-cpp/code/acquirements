// class Student header

# pragma once

using std::string;

class Student {
private:
  string firstName;
  string lastName;
  int age;
  string address;
  string city;
  int phone;
public:
  Student();
  Student(string, string, int, string, string, int);
  ~Student();

  void setFirstName(string);
  void setLastName(string);
  void setAge(int);
  void setAddress(string);
  void setCity(string);
  void setPhone(int);

  string getFirstName();
  string getLastName();
  int getAge();
  string getAddress();
  string getCity();
  int getPhone();

  void SitInClass();
};
