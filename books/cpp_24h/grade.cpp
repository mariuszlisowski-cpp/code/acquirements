#include <iostream>


int main()
{
  char grade;
  std::cout << "Jaką dostałeś ocenę? >";
  std::cin >> grade;

  switch (grade)
  {
    case '1':
      std::cout << "Jedynka\n";
      break;
    case '2':
      std::cout << "Dwójka\n";
      break;
    case '3':
      std::cout << "Trójka\n";
      break;
    case '4':
      std::cout << "Czwórka\n";
      break;
    case '5':
      std::cout << "Piątka\n";
      break;
    case '6':
      std::cout << "Szóstka\n";
      break;
    default:
      std::cout << "Ocena poza zakresem!\n";
      break;
  }
  return 0;
}
