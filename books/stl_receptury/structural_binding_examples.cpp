#include <chrono>
#include <iostream>
#include <string>
#include <vector>

std::pair<int, int> divide_reminder(int divident, int divisor);     // function prototype returning a pair

std::tuple<std::string,
           std::chrono::system_clock::time_point, unsigned>
stock_info(const std::string& name);                                // function prototype returning a tuple

struct employee {
    unsigned id;                                                    // unsigned int datatype
    std::string name;
    std::string role;
    unsigned salary;
};

int main() {
    // regualr
    const auto result (divide_reminder(16, 3));
    std::cout << result.first 
              << " plus reszta z dzielenia "
              << result.second
              << std::endl;

    // c++17
    auto [fraction, reminder] = divide_reminder(16, 3);
    std::cout << fraction
              << " plus reszta z dzielenia "
              << reminder
              << std::endl;
    
    const auto [company, valid_time, price] = stock_info("NASDAQ");

    employee emp;
    emp.id = 1;
    emp.name = "Smith";
    emp.role = "salesman";
    emp.salary = 123000;
    const auto [id, name, role, salary] = emp;
    std::cout << "Id: " << id
              << "Name: " << name
              << "Role: " << role
              << "Salary: " << salary
              << std::endl;
    
    std::vector<std::tuple<int, float, long>> tuples;
    tuples.push_back({9, 3.14, 1'000'000'000'000});
    tuples.push_back({7, 1.6, 1'000'000});
    for (const auto& [integer, floating, large] : tuples) {
        std::cout << integer << ' '
                  << floating << ' '
                  << large << ' '
                  << std::endl;
    }

    return 0;
}
