#include <iostream>
#include <string>

template <typename T> void sizeOfType(T);

int main() {
  short s = 2;
  int i = 5;
  float f = 2.53;
  double d = 3.14;
  long l = 9;
  long long ll = 4;
  auto str = std::string("this is a test");

  sizeOfType(s);
  sizeOfType(i);
  sizeOfType(f);
  sizeOfType(d);
  sizeOfType(l);
  sizeOfType(ll);
  sizeOfType(str);

  return 0;
}

template <typename T>
void sizeOfType(T a) {
  std::cout << "value " << a << " : bytes " << sizeof(a) << std::endl;
}
