// Vector of vectors initialization

#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n(3), m(4);
  // n vectors with m elements
  vector<vector<int> > matrix(n, vector<int>(m));

  // populate with random numbers
  srand(time(0));
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      matrix[i][j] = rand() % 10; // 0-9
    
    // size
    int sizeR = matrix.size(); // vectors in vector (rows)
    int sizeC0 = matrix[0].size(); // elements of the first vector (columns)
  
    // display
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++)
      cout << matrix[i][j] << " ";
    cout << endl;
  }

  return 0;
}
