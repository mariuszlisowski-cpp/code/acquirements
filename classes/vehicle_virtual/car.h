// derived class header

#pragma once

using std::string;

class Car : public Vehicle {
protected:
  int seats;
public:
  // constructors
  Car();
  Car(const int); // derived class parameter
  Car(const string, const int, const int); // base & derived class parameters

  // virtual destructor
  virtual ~Car(); // must be virtual

  // overriden function
  virtual void display() const; // good practice to make it virtual as well

  // overriden pure function
  virtual void drive();
};
