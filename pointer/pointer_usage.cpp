#include <iostream>

using namespace std;



int main()
{
  int liczba = 5;
  int *pLiczba = nullptr;  // deklaracja pustego wskaźnika
  pLiczba = &liczba; // przypisanie adresu zmiennej l do wskaźnika pl
  cout << endl << "Adres wskaźnika 'pLiczba': " << pLiczba << endl; // wyświetla adres wskaźnika
  cout << "Wartość wskaźnika 'pLiczba': " << *pLiczba << endl << endl; // wyłuskanie wartości wskaźnika

  long x = 65535;
  long *pX = &x; // deklaracja wskaźnika z jednoczesnym przypisaniem adresu
  long wartosc;
  wartosc = *pX; // wyłuskanie wartości wskaźnika do zmiennej
  *pX = -65535; // przypisanie zmiennej x nowej wartości poprzez wskaźnik
  cout << "Adres wskaźnika 'pX': " << pX << endl; // wyświetla adres wskaźnika
  cout << "Wartość wskaźnika 'pX': " << *pX << endl << endl; // wyłuskanie wartości
  cout << "Wartość wskaźnika 'pX' w nowej zmiennej: " << wartosc << endl;
  cout << "Adres nowej zmiennej: " << &wartosc << endl << endl; // adres w pamięcin
  cout << "Wartość wskaźnika 'pX' po zmianie: " << *pX << endl;
  cout << "Wartość zmiennej 'x' po zmianie: " << x << endl << endl;

  return 0;
}
