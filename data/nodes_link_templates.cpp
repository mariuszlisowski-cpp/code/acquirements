// nodes link template

#include <iostream>

using namespace std;

template<class T>
class Node {
public:
    Node(T val = 0):value(val),Next(nullptr) {};

    T value;
    Node<T> * Next;
};

template<class T>
void printNodes(Node<T> *);

int main() {
    /// adding values
    // +------+------+
    // |  7   | NULL |
    // +------+------+
    Node<float> * node1 = new Node<float>(7.5);

    // +------+------+
    // |  14  | NULL |
    // +------+------+
    Node<float> * node2 = new Node<float>(14.5);
    // +------+------+
    // |  21  | NULL |
    // +------+------+
    Node<float> * node3 = new Node<float>(21.5);

    /// linking nodes
    // +------+------+  +------+------+
    // |  7   |   +---->|  14  | NULL |
    // +------+------+  +------+------+
    node1->Next = node2;
    // +------+------+  +------+------+  +------+------+
    // |  7   |   +---->|  14  |   +---->|  21  | NULL |
    // +------+------+  +------+------+  +------+------+
    node2->Next = node3;

    printNodes(node1);
}

template<class T>
void printNodes(Node<T> * node) {
    while (node != NULL) {
        cout << node->value << " -> ";
        node = node->Next;
    }
    cout << "NULL" << endl;
}
