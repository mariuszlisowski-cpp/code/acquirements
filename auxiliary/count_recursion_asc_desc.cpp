#include <iostream>

using namespace std;

void increment(int i) {
    if (i >= 10) {
        return;    
    }
    cout << i << '|';
    increment(++i);
    cout << i << '|';
}

int main()
{
    increment(1);

    return 0;
}
