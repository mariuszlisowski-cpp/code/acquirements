#include <ctime>
#include <iostream>

int main() {
    // current date/time based on current system
    std::time_t now = std::time(0);

    // convert now to string form
    char* dt = std::ctime(&now);
    std::cout << "The local date and time: " << dt << std::endl;

    // convert now to tm struct for UTC
    tm* gmtm = std::gmtime(&now);
    dt = std::asctime(gmtm);
    std::cout << "The UTC date and time:" << dt << std::endl;
}
