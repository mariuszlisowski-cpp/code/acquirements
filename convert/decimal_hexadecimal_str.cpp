#include <iostream>
#include <sstream>

std::string decimalToHexadecimal(long decimal) {
    long hex_base{16};
    int remainder{};
    std::ostringstream oss;
    const char* hex = "0123456789ABCDEF";                       // 0-15 used as reminder index
    while (decimal > 0) {
        remainder = decimal % hex_base;
        std::cout << remainder << std::endl;                    // verbose
        oss << hex[remainder];                                  // get hex symbol (0-F)
        decimal = decimal / hex_base;
    }
    std::string str(oss.str());
    std::reverse(str.begin(), str.end());

    return str;
}

int main() {
    std::cout << decimalToHexadecimal(65534) << std::endl;

    return 0;
}
