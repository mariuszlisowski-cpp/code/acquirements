// palindrome long sentence

#include <algorithm>
#include <codecvt>  // std::codecvt_utf8_utf16
#include <cwctype>  // ::iswspace, ::iswpunct
#include <iostream>
#include <locale>  // std::wstring_convert
#include <string>



bool is_palindrome(const std::string& s) {
    if (s.empty()) {
        return true;
    }

    std::string str{ s };

    // verbose
    std::cout << str << '\n';

    // leaving alpha & digits only
    str.erase(remove_if(std::begin(str),
                        std::end(str),
                        [](char ch) {
                            return !std::isalpha(ch) &&
                                   !std::isdigit(ch);
                        }),
              str.end());
    // changing letter case
    std::transform(std::begin(str),
                   std::end(str),
                   std::begin(str),
                   ::tolower);
    // verbose
    std::cout << str << '\n';
    // comparing while reversing
    return (std::mismatch(str.begin(),
                          str.end(),
                          str.rbegin()))
               .first == str.end();
}


void test(const std::string& s) {
    std::cout << (is_palindrome(s) ? "Is" : "Is not")
               << " a palindrome\n";
}

int main() {
    std::string s{
        "Are we not pure? “No, sir!” Panama’s moody Noriega brags."
        "It is garbage!” Irony dooms a man—a prisoner up to new era."
    };

    test(s);

    return 0;
}