#include <algorithm>
#include <chrono>
#include <functional>
#include <iterator>
#include <iostream>
#include <vector>
#include <random>

template<typename T>
void print(const std::vector<T>& vec) {
    std::copy(vec.begin(), vec.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 10;
    std::vector<int> vec(size);

    std::mt19937 gen(std::random_device{}());
    std::uniform_int_distribution<> uid(1, 9);
    
    /* fill randomly using lambda */
    std::generate(vec.begin(), vec.end(),
                  [&](){
                      return uid(gen);                              // already generated integer
                  });
    print(vec);

    /* fill randomly using binding */
    std::generate(vec.begin(), vec.end(),
                  std::bind(uid, std::ref(gen)));                   // bind distribution to generator
                                                                    // generator becomes an argument of distribution
    print(vec);

    return 0;
}
