/*
Char find pointer
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

unsigned charCounter(const char*, char);

int main() {
  char sentence[20] = "something new";
  char* sent = "nothing at all";

  cout << "Inside \"" << sentence << "\" there are "
       << charCounter(sentence, 'n') << " 'n' letters" << endl;
  cout << "Inside \"" << sent << "\" there are "
       << charCounter(sent, 'a') << " 'a' letters" << endl;

  return 0;
}

unsigned charCounter(const char* str, char c) {
  unsigned count = 0;
  // continue until end of the string ('\0')
  while (*str) {
    if (*str == c)
      count++;
    str++; // next char
  }
  return count;
}
