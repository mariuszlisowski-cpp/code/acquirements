// Queue class implementation

#include <iostream>
#include "queue.h"

using namespace std;

void Queue::enqueueCharacter(char ch) {
    QueueElement* newElement = new QueueElement(ch);
    if (first == NULL)
      first = last = newElement;
    else {
      last->after = newElement; // ???
      last = newElement;
    }
}

char Queue::dequeueCharacter() {
  char ch;
  if (first != NULL) {
    QueueElement* temp = first;
    ch = temp->data;
    first = temp->after;
    delete temp;
  }
  return ch;
}

void Queue::showQueue() {
  QueueElement* temp = first; // must be temp (first cannot be changed)
  while (temp != NULL) {
    cout << temp->data << "-";
    temp = temp->after;
  }
  delete temp;
  cout << endl;
}
