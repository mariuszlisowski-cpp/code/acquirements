/*
Skarbonka
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <string>

using namespace std;

class Skarbonka {
private:
  float money = 0;
public:
  Skarbonka() {};
  void insertMoney(float amount) {
    money += amount;
  }
  void withdrawMoney(float amount) {
    cout << setprecision(3);
    if (money == 0 && amount != 0) {
      cout << "Skarbonka pusta :(" << endl;
      cin.get();
      cin.ignore(1, '\n');
    }
    else {
      if (amount <= money)
        money -= amount;
      else {
        cout << "W skarbonce jest za mało."
                 " Zostało " << money << " zł." << endl;
        cin.get();
        cin.ignore(1, '\n');
      }
    }
  }
  void showMoney() {
    if (money == 0) {
      cout << "Skarbonka pusta :(" << endl;
      cin.get();
      cin.ignore(1, '\n');
    }
    else
      cout << "W skarbonce zostało " << money << " zł." << endl;
      cin.get();
      cin.ignore(1, '\n');
  }
  float balance() {
    return money;
  }
};

class Domownik : public Skarbonka {
private:
  int age;
public:
  string name, surname;
  Domownik() {};
  void addDweller(string aName, string aSurname, int aAge) {
    name = aName;
    surname = aSurname;
    age = aAge;
  }
  void showDweller() {
    cout << name << " " << surname
         << " posiada w skarbonce " << this->balance() << " zł." << endl;
    cin.get();
    cin.ignore(1, '\n');
  }
  void nameDweller() {
    cout << name << " " << surname << ", lat " << age << "." << endl;
  }
};

void addDwellers(int &, Domownik*);
void showDwellers(int, Domownik*);

int main()
{
  Domownik dwellers[10];

  int dwellersNo = 3;
  dwellers[0].addDweller("Mateusz", "Lisowski", 11);
  dwellers[1].addDweller("Miłosz", "Lisowski", 8);
  dwellers[2].addDweller("Ewelina", "Lisowska", 39);

  char choice;
  string s;
  while (true) {
    cout << endl << "Wybór opcjii... (ENTER kończy)" << endl;
    cout << "1. Dodaj domownika" << endl
         << "2. Pokaż domowników" << endl << ": ";
    cin.get(choice);
    if (choice == '\n')
      break;
    else {
      cin.ignore(1, '\n');
      switch (choice) {
        case '1': addDwellers(dwellersNo, dwellers);
        break;
        case '2': showDwellers(dwellersNo, dwellers);
        break;
        default: cout << "Spróbuj jeszcze raz..." << endl;
      }
    }
  }

  return 0;
}

void showDwellers(int number, Domownik* dwell) {
  char choice;
  int num;
  while (true) {
    cout << endl << "Domownicy..." << endl;
    for (int i = 0; i < number; i++) {
      cout << i+1 << ". ";
      dwell[i].nameDweller();
    }
    cout << endl<< "Wybierz domownika..." << endl << ": ";
    cin.get(choice);
    num = int(choice) - 49;
    if (choice == '\n')
      break;
    else
      dwell[num].showDweller();

    float amount;
    while (true) {
      cout << dwell[num].name << "..." << endl;
      cout << "1. Oszczędza" << endl
           << "2. Wypłaca ze skarbonki" << endl << ": ";
      cin.get(choice);
      if (choice == '\n')
        break;
      else {
        cin.ignore(1, '\n');
        switch (choice) {
          case '1': cout << "Ile wpłaca: ";
                    cin >> amount;
                    dwell[num].insertMoney(amount);
                    break;
          case '2': cout << "Ile wypłaca: ";
                    cin >> amount;
                    dwell[num].withdrawMoney(amount);
                    break;
          default: cout << "Spróbuj jeszcze raz..." << endl;
        }

      }
    }
  }
}

void addDwellers(int &dwellNo, Domownik* dwell) {
  string name, surname;
  int age;
  cout << "Imię domownika: ";
  cin >> name;
  cout << "Nazwisko domownika: ";
  cin >> surname;
  cout << "Wiek domownika: ";
  cin >> age;
  dwell[dwellNo].addDweller(name, surname, age);
  dwellNo++;
  cin.ignore(1, '\n');
}
