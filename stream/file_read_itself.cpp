/*
File read itself
~ plik czyta sam siebie i wyświetla swoją zawartość
*/

#include <iostream>
#include <fstream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;
using std::ifstream;

int main() {

  ifstream file;
  file.open("file_read_itself.cpp");

  // any problems opening file?
  if (!file.is_open()) {
      cout << "File opening error!" << endl;
      exit(EXIT_FAILURE);
  }

  // file read
  char ch;
  int count = 0;
  while (file >> ch) {
    cout << ch;
    ++count;
  }

  // file end or read error
  if (file.eof()) {
    cout << endl << "End of file!" << endl;
    cout << "File contains " << count << " characters." << endl;
  }
  else if (file.fail())
    cout << endl << "Data read error!" << endl;
  else
    cout << "Unknown error!" << endl;

  file.close();

  return 0;
}
