#include <iostream>
#include <map>

int main() {
    std::map<char, int> m{{'a', 1}, {'b', 2}};

    // regular if
    auto it = m.find('b');
    if (it != m.end()) {
        std::cout << "> found: " << it->first << ' '
                  << it->second << std::endl;
    }
    auto character = it->first;                         // iterator still valid

    // c++17 (limited scope of iterator)
    if (auto it(m.find('a')); it != m.end()) {
        std::cout << "> found: " << it->first << ' '
                  << it->second << std::endl;
    } else {
        std::cout << it->first 
                  << " not found!" << std::endl;        // iterator still valid
    }                                                   // iterator out of scope

    return 0;
}
