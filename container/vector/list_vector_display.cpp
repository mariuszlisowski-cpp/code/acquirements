/*
List vs Vector - display
*/

#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>
#include <list>

using namespace std;

/* deklaracja funkcji */
void displayVector(vector<int>);
void displayList(list<int>);
vector<int> divideContainer(vector<int>, vector<int> &);
list<int> divideContainer(list<int>, list<int> &);

/* deklaracja stałych */
const short divider = 45; // miejsce podziału kontenerów
const short cap = 18; // pojemność kontenerów
const short minn = 10, maxx = 99; // zakres liczb losowych

int main()
{
  short t;
  int losowa;

  /* inicjalizacja generatora liczb pseudolosowych */
  srand(time(0));

  /* zapełnienie wektora liczbami */
  vector<int> v; // wektor zawierający zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    v.push_back(losowa); // losowe liczby do wektora
    t++;
  }

  /* zapełnienie listy liczbami */
  list<int> l; // lista zawierająca zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    l.push_back(losowa); // losowe liczby do listy
    t++;
  }

  /* wyświetlenie wektora 'v' */
  cout << "Wartości losowe kontenera 'vector'..." << endl;
  displayVector(v);
  cout << endl;
  cout << "A posortowane rosnąco..." << endl;
  sort(v.begin(), v.end()); // implementacja sortowania
  displayVector(v);
  cout << endl << endl;

  /* wyświetlenie listy 'l' */
  cout << "Wartości losowe kontenera 'list'..." << endl;
  displayList(l);
  cout << endl;
  cout << "A posortowane rosnąco..." << endl;
  l.sort(); // odmienna implementacja sortowania
  displayList(l);
  cout << endl << endl;

  /* skopiowanie z wektora 'v' elemenów większych niż... do wektora 'vec' */
  vector<int> vec; // deklaracja nowego wektora
  v = divideContainer(v, vec); // wektor 'vec' zwracany przez referencję

  /* skopiowanie z listy 'l' elemenów większych niż... do listy 'lis' */
  list<int> lis; // deklaracja nowej listy
  l = divideContainer(l, lis); // lista 'lis' zwracana przez referenjcę

  /* wyświetlenie zmodyfikowanego wektora 'v' i nowego 'vec' */
  cout << "Kontener 'vector'..." << endl;
  cout << "Wartości większe od " << divider << "..." << endl;
  displayVector(vec);
  cout << endl;
  cout << "Wartości mniejsze lub równe " << divider << "..." << endl;
  displayVector(v);
  cout << endl << endl;

  /* wyświetlenie zmodyfikowanej listy 'l' i nowej 'lis' */
  cout << "Kontener 'list'..." << endl;
  cout << "Wartości większe od " << divider << "..." << endl;
  displayList(lis);
  cout << endl;
  cout << "Wartości mniejsze lub równe " << divider << "..." << endl;
  displayList(l);
  cout << endl << endl;

  return 0;
}

/* wyświetlenie wektora */
void displayVector(vector<int> aV)
{
  vector<int>::const_iterator iter = aV.begin(); // deklaracja iteratora
  while (iter != aV.end())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość wektora
    ++iter;
  }
}

/* wyświetlenie listy */
void displayList(list<int> aL)
{
  list<int>::const_iterator iter = aL.begin(); // deklaracja iteratora
  while (iter != aL.end())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość listy
    ++iter;
  }
}

vector<int> divideContainer(vector<int> aV, vector<int> &aVec)
{
  vector<int>::const_iterator iter = aV.begin(); // deklaracja iteratora
  while (iter != aV.end())
  {
    if (*iter > divider)
    {
      aVec.push_back(*iter);
      iter = aV.erase(iter); // zwraca iterator za elementem usuwanym
    }
    else ++iter;
  }
  return aV;
}

list<int> divideContainer(list<int> aL, list<int> &aLis)
{
  list<int>::const_iterator iter = aL.begin(); // deklaracja iteratora
  while (iter != aL.end())
  {
    if (*iter > divider)
    {
      aLis.push_back(*iter);
      iter = aL.erase(iter); // zwraca iterator za elementem usuwanym
    }
    else ++iter;
  }
  return aL;
}
