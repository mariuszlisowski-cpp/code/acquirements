/*
~ dzielenie zdania na wyrazy
*/

#include <iostream>
#include <cctype> // do instrukcji 'isspace' wyszykującą spacje
#include <vector>

using namespace std;

vector<string> split(const string &);

int main()
{
  string s;
  cout << "Napiszesz jakieś zdanie?" << endl << ": ";
  getline(cin, s);

  // podzielenie zdania na słowa
  vector<string> w;
  w = split(s);

  // wyświetlenie kontenera ze słowami
  for (int i = 0; i < w.size(); i++)
    cout << w[i] << endl;

  return 0;
}

// funkcja wycinająca słowa ze zdania
vector<string> split(const string &aS)
{
  typedef string::size_type string_size;
  vector<string> words;
  string_size i = 0, j;

  while ( i != aS.size()) {

    while ( (i != aS.size()) && isspace(aS[i]) )
      i++; // indeks pierwszej spacji

    j = i;
    while ( (j != aS.size()) && !isspace(aS[j]) )
      j++; // indeks spacji po wyrazie

    if (i != j) {
      words.push_back(aS.substr(i, j-i));
      i = j;
    }
  }
  return words;
}
