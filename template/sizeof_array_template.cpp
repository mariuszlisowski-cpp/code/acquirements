#include <iostream>

template<typename T, size_t SIZE>
size_t getSize(T (&)[SIZE]) {
    return SIZE;
}

struct foo_t {
    int ball;
};

int main()
{
    int foo3[] = {1, 2, 3};
    foo_t foo4[] = {{1},{2},{3},{4}};
    foo_t foo5[] = {{1},{2},{3},{4},{5}};

    std::cout << getSize(foo3) << std::endl;
    std::cout << getSize(foo4) << std::endl;
    std::cout << getSize(foo5) << std::endl;

    return 0;
}