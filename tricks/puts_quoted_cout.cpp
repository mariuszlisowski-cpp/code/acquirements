#include <iomanip>
#include <iostream>

int main() {
    std::puts("Hello world!");

    std::cout << std::quoted("Hello world!") << std::endl;

    return 0;
}
