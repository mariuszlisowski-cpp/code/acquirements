// binary to hex

#include <iostream>
#include <vector>
#include <string>

using namespace std;

string binaryToHex(string binary) {
    string str;
    int counter {4};
    vector<string> nibbles;
    
    // divide input string into nibble strings
    for (auto it = binary.rbegin(); it != binary.rend(); ++it) {
        // completing nibbles
        str += *it;
        --counter;
        // is full nibble completed
        if (!counter) {
            nibbles.push_back(str);
            str.clear();
            counter = 4;
        }
        // incomplete nibble (adding zeroes)
        else if (next(it) == binary.rend()) {
            // nibble must have four characters
            while (str.size() != 4)
                str += '0'; // add trailing zeroes
            nibbles.push_back(str);
            str.clear();
        }
    }
    
    // verbose display
    for (auto e : nibbles)
        cout << e << endl;
    
    int sum {};
    string output;
    // iterate through vector
    for (int i = 0; i < nibbles.size(); ++i) {
        // iterate through string
        for (int j = 0; j < nibbles[i].size(); ++j) {
            // adding positions of ones
            if (nibbles[i][j] == '1') {
                if (j == 0) sum += 1;
                if (j == 1) sum += 2;
                if (j == 2) sum += 4;
                if (j == 3) sum += 8;
            }
        }
        // converting above digit 9
        if (sum == 10) str = 'A';
        else if (sum == 11) str = 'B';
        else if (sum == 12) str = 'C';
        else if (sum == 13) str = 'D';
        else if (sum == 14) str = 'E';
        else if (sum == 15) str = 'F';
        else str = ::to_string(sum);
        // verbose
        cout << sum << " -> " << str << endl;
        // do not add leading zeros
        if (sum)
            output = str + output;
        // clean before next iteration
        sum = 0;
    }
    // return zero if input zero
    if (!output.empty())
        return output;
    else
        return "0";
}

int main() {
    string s {"01110100001110111"};

    cout << binaryToHex(s) << endl;
    
    return 0;
}