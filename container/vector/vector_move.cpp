/*
Przesuwa wektor w lewo i prawo o zadaną ilość pozycji
*/

#include <iostream>
#include <vector>

using namespace std;

void moveRight(vector<char>&, int);
void moveLeft(vector<char>&, int);
void showVector(vector<char>&);
vector<char> stringToVector(string);

int main() {
  string s = "---/------";
  int const r = 4; // przesunięcie w prawo
  int const l = 6; // przesunięcie w lewo

  vector<char> v = stringToVector(s);

  cout << "Przesuń:\t" << s << endl;

  cout << r << " w prawo:\t";
  moveRight(v, r);
  showVector(v);

  cout << l << " w lewo:\t";
  moveLeft(v, l);
  showVector(v);

  return 0;
}

/* przesuń wektor w prawo */
void moveRight(vector<char>& aV, int p) {
  char t;
  while (p--) {
    t = aV[aV.size()-1]; // ostatni element zapamiętany
    aV.erase(aV.end()-1); // usuń ostatni element
    aV.insert(aV.begin(), 1, t); // wstaw 1 element o wartości 't' na początek
  }
}

/* przesuń wektor w lewo */
void moveLeft(vector<char>& aV, int p) {
  char t;
  while (p--) {
    t = aV[0]; // pierwszy element zapamiętany
    aV.erase(aV.begin()); // usuń pierwszy element
    aV.push_back(t); // wstaw 1 element o wartości 't' na początek
  }
}

/* wyświetl wektor */
void showVector(vector<char>& aV) {
  typedef vector<char>::iterator iter;
  for (iter it = aV.begin(); it != aV.end(); ++it)
    cout << *it;
  cout << endl;
}

/* zamień łańcuch na wektor */
vector<char> stringToVector(string aS) {
  vector<char> vec;
  for (int i = 0; i < aS.length(); i++)
    vec.push_back(aS[i]); // znaki łańcucha do wektora
  return vec;
}
