// class Student implementation

#include <iostream>
#include "student.h"

using std::cout;
using std::endl;
using std::string;

Student::Student() {}
Student::Student(string fName, string lName, int age, string address, string city, int phone):firstName(fName), lastName(lName), age(age), address(address), city(city), phone(phone) {}
Student::~Student() {}

void Student::setFirstName(string fName) { firstName = fName; }
void Student::setLastName(string lName) { lastName = lName; }
void Student::setAge(int age) { age = age; }
void Student::setAddress(string address) { address = address; }
void Student::setCity(string city) { city = city; }
void Student::setPhone(int phone) { phone = phone; }

string Student::getFirstName() { return firstName; }
string Student::getLastName() { return lastName; }
int Student::getAge() { return age; }
string Student::getAddress() { return address; }
string Student::getCity() { return city; }
int Student::getPhone() { return phone; }

void Student::SitInClass() {
  cout << "Sitting in main theater" << endl;
}
