#include <iostream>
#include <memory>
 
struct Node {
    Node(int data) : data{data}, next{nullptr} {}
    std::unique_ptr<Node> next;
    int data;
};
 
struct List {
    List() : head{nullptr} {};
    ~List() {
        // clear();                                                        // must be used to avoid stack overflow
    }

    /* inserts to the head of the list for performance reasons
       this is an O(1) operation
          node          node          node                   temp
       |data|next|-> |date|next|-> |data|next|   <===push |data|next|
                                      head                         */
    void push(int data) {
        auto temp{std::make_unique<Node>(data)};
        if (!head) {
            head = std::move(temp);
        } else {
            temp->next = std::move(head);
            head = std::move(temp);
        }
    }

private:
    std::unique_ptr<Node> head;
    void clear() {                                                          // not used
        while(head) {
            head = std::move(head->next);
        }
    }
};

int main() {
    List list;

    for (int i = 0; i < 200'000; ++i) {                                     // can cause stack overflow
        list.push(i);                                                       // for too many nodes
    }                                                                       // but should work for 20'000 nodes

    return 0;                                                               // destroyed recursively
}                                                                           // causing stack overflow
