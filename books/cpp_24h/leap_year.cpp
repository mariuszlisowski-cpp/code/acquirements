#include <iostream>

bool isLeapYear(int);

int main()
{
  int input;
  std::cout << "Podaj rok: ";
  std::cin >> input;

  if (isLeapYear(input))
    std::cout << input << " jest rokiem przestępnym.\n\n";
  else
    std::cout << input << " nie jest rokiem przzestępnym.\n\n";
  return 0;
}

bool isLeapYear(int year)
{
  if (year % 4 == 0)
  {
    if (year % 100 ==0)
    {
      if (year % 400 ==0) return true;
      else return false;
    }
    else return true;
  }
  else return false;
}
