#include <iostream>
#include "../inc/bst.hpp"

BSTNode::BSTNode() {
    Left = nullptr;
    Right = nullptr;
    Parent = nullptr;
}
BST::BST():Root(nullptr) {}

BSTNode * BST::insert(BSTNode * node, int key) {
    // verbose
    std::cout << "------ Function ENTER ------" << std::endl;
    static int i {1}, j {1};
    // root root or new leaf
    if (!node) {
        node = new BSTNode;
        node->Key = key;
        // verbose
        std::cout << "New node created with key " << node->Key << std::endl;
    }
    // larger keys at the right branch
    else if (key > node->Key) {
        // verbose
        std::cout << "Recursion A" << i++ << " ENTER " << "at key " << node->Key << std::endl;
        node->Right = insert(node->Right, key); // RECURSION A
        // verbose
        std::cout << "Recursion A" << --i << " EXIT  " << "at key " << node->Key << std::endl;
        node->Right->Parent = node;
    }
    // smaller keys at the left branch
    else if (key <= node->Key) {
        // verbose
        std::cout << "Recursion B" << j++ << " ENTER " << "at key " << node->Key << std::endl;
        node->Left = insert(node->Left, key); // RECURSION B
        // verbose
        std::cout << "Recursion B" << --j << " EXIT  " << "at key " << node->Key << std::endl;
        node->Left->Parent = node;
    }
    // verbose
    std::cout << "* Function EXIT at node " << node->Key << " *" << std::endl;
    return node;
}

void BST::insert(int key) {
    Root = insert(Root, key);
}
