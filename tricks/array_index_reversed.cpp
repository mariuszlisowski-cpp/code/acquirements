#include <iostream>

template <typename T>
void print_array (T &arr) {
    for (auto& el : arr) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
};

int main() {
    int arr[]<%                                                     // '<%' == {
        0, 1, 2, 3, 4, 5
    %>;                                                             // '%>' == }

    5[arr] = 42;                                                    // equivalent of *(2 + arr) or *(arr + 2)
    print_array(arr);

    0<:arr:> = 24;                                                  // '<:' == '['
    print_array(arr);                                               // ':>' == ']'

    return 0;
}
