#include <iostream>

using namespace std;

class Rectangle
{
public:
  Rectangle();  // deklaracja konstruktora
  ~Rectangle(); // deklaracja destruktora
  void setLength(int length) { itsLenght = length; }
  int getLength() const { return itsLenght; }
  void setWidth(int width) { itsWidth = width; }
  int getWidth() const { return itsWidth; }
private:
  int itsLenght;
  int itsWidth;
};

Rectangle::Rectangle() // definicja konstruktora
{
  itsWidth = 5;   // inicjalizacja wartości początkowych zmiennych klasy
  itsLenght = 10;
}

Rectangle::~Rectangle() // definicja destruktora
{}

int main()
{
  Rectangle *pRect = new Rectangle;
  const Rectangle *pConstRect = new Rectangle; // wartość wskaźnika niezmienna
  Rectangle * const pConstPtr = new Rectangle; // wskaźnik nie może wskazywać na nic innego (ma ustalony adres)

  /*
  const int *pOne;          // stała wartość wskaźnika
  int * const pTwo;         // stały adres wskaźnika
  const int * const pThree; // stała wartośc i adres
  */

  cout << "\nRect ma szerokość " << (*pRect).getWidth();
  cout << "\nConstRect ma szerokość " << (*pConstRect).getWidth();
  cout << "\nConstPtr ma szerokość " << (*pConstPtr).getWidth() << endl;

  (*pRect).setWidth(10);
  (*pConstPtr).setWidth(20);
  // (*pConstRect).setWidth(9); // operacja niedozwolona, bo pConstRect = const

  cout << "\nRect ma szerokość " << (*pRect).getWidth();
  cout << "\nConstRect ma szerokość " << (*pConstRect).getWidth();
  cout << "\nConstPtr ma szerokość " << (*pConstPtr).getWidth() << endl;

  return 0;
}
