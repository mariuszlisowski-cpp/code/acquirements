#include <iostream>

using namespace std;

class Rectangle
{
public:
  Rectangle();  // deklaracja konstruktora
  ~Rectangle(); // deklaracja destruktora
  void setLength(int length) { this->itsLenght = length; }
  int getLength() const { return this->itsLenght; }
  void setWidth(int width) { this->itsWidth = width; }
  int getWidth() const { return this->itsWidth; }
private:
  int itsLenght;
  int itsWidth;
};

Rectangle::Rectangle() // definicja konstruktora
{
  itsWidth = 5;
  itsLenght = 10;
}

Rectangle::~Rectangle() // definicja destruktora
{}

int main()
{
  Rectangle theRect;

  cout << "\ntheRect ma " << theRect.getLength() << " cm długości oraz " << theRect.getWidth() << " cm szerokości.";

  theRect.setLength(20);
  theRect.setWidth(10);

  cout << "\ntheRect ma " << theRect.getLength() << " cm długości oraz " << theRect.getWidth() << " cm szerokości.";

  cout << endl;
  return 0;
}
