// read & write text file

#include <iostream>
#include <fstream>

using namespace std;

void fileReadText(string);
void fileWriteText(string);

int main() {
  const string filename = "file.txt";

  fileWriteText(filename);
  cout << endl;
  fileReadText(filename);

  return 0;
}

void fileReadText(string filename) {
  ifstream ifile(filename);
  if (ifile.is_open()) {
    string line;
    cout << "Reading text form file..." << endl;
    while (getline(ifile, line)) {
      cout << line << endl;
    }
    ifile.close();
    cout << "Finished reading text from " << filename << endl;
  }
  else
    cerr << "Couldn't open " << filename << " for reading" << endl;
}

void fileWriteText(string filename) {
  ofstream ofile(filename);
  cout << "Writing text to file..." << endl;
  if (ofile.is_open()) {
    ofile << "Here is line 1" << std::endl;
    ofile << "Here is line 2" << std::endl;
    ofile << "Here is line 3" << std::endl;
    ofile.close();
    cout << "Finished writing text to " << filename << endl;
  }
  else
  cerr << "Couldn't open " << filename << " for reading" << endl;
}
