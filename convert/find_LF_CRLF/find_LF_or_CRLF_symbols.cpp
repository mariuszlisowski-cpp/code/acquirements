#include <iostream>
#include <string>

int main() {
    std::string str = "A line with LF\n";
    // std::string str = "A line with CRLF\r\n";

    auto posLF = str.find("\n");         // LF
    auto posCRLF = str.find("\r\n");     // CRLF

    if (posCRLF != std::string::npos) {
        std::cout << "Line break type CRLF" << std::endl;
    } else if (posCRLF == std::string::npos && posLF != std::string::npos) {
        std::cout << "Line break type LF" << std::endl;
    }

    return 0;
}
