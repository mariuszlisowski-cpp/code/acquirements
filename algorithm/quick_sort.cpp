// quick sort
// O(nlogn) [worst case O(n*n)]

#include <iostream>

using namespace std;

void quickSort(int* arr, int left, int right) {
    // bad arguments
    if (left >= right)
        return;
    // pivot in the middle
    int pivot = arr[(left + right) / 2];
    // indexes before the left and past the right
    int leftIndex = left - 1, rightIndex = right + 1;
    // smaller numbers | pivot | greater numbers
    while(true) {
        // pre-increment used (0, arrSize - 1)
        while (pivot > arr[++leftIndex]);
        while (pivot < arr[--rightIndex]);
        // if still mixed
        if (leftIndex <= rightIndex)
            swap(arr[leftIndex], arr[rightIndex]);
        // two subarrays ready
        else break;
    }
    // if (rightIndex > left)
        quickSort(arr, left, rightIndex);
    // if (leftIndex < right)
        quickSort(arr, leftIndex, right);
}

int main() {
    int arr[] {9, 1, 2, 4, 5, 7, 8, 6, 3};
    int arrSize = sizeof(arr) / sizeof(*arr);

    quickSort(arr, 0, arrSize - 1);

    for (const auto& e : arr)
        cout << e << " ";

    return 0;
}