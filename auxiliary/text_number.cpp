/*
Konwersja tekstu na liczbę
*/

#include <iostream>
#include <sstream>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;

int main()
{
  while (true) {
    string s;
    int num;
    cout << "Tekst do konwersji na liczbę: ";
    getline(cin, s);
    stringstream input(s);
    if (input >> num) {
      cout << "Konwersja udana... wynik to: " << num << endl;
      break;
    }
    else
      cout << "Niepowodzenie... niepoprawny format!" << endl;
  }
  return 0;
}
