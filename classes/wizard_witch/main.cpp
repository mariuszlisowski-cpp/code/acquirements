// base & derived classes objects
// #pointer

#include <iostream>
#include "class.h"

using namespace std;

void introduceItself(Creature * creature) {
    creature->whoAreU();
}

int main() {
    // derived classes' objects
    Wizard lancelot;
    Witch jaga;
    // direct methods
    lancelot.whoAreU();
    jaga.whoAreU();

    // pointer to an existing object
    Creature * monster;
    monster = &lancelot;
    introduceItself(monster);
    monster = &jaga;
    introduceItself(monster);

    // pointer to a new object
    Creature * dragon;
    dragon = new Wizard;
    introduceItself(dragon);
    dragon = new Witch;
    introduceItself(dragon);
    delete dragon;

    return 0;
}
