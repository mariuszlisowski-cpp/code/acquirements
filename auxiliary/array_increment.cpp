// array incrementation

#include <iostream>

using namespace std;

int main() {
    int * array = new int [10];

    array[0] = 10;
    array[1] = 20;
    array[2] = 30;

    // array[i]++ SAME AS (*(array+i))++ (increments the value)
    array[0] = array[0] + 1;
    cout << array[0] << endl;

    array[1]++;
    cout << array[1] << endl;

    (*(array + 2))++;
    cout << array[2] << endl;

    delete [] array;

    return 0;
}
