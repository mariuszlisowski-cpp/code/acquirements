#ifndef NODE_H
#define NODE_H

class Node {
public:
    Node(int);

    int value;
    Node * under;
};

inline Node::Node(int val):value(val), under(nullptr) {}

#endif
