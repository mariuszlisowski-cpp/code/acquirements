// permutations generator
// ~ a word or phrase formed by rearranging the letters of a different
// word or phrase, typically using all the original letters exactly once
// ~ no meaning

#include <iostream>
#include <algorithm>

using namespace std;

void displayAllPermutations(string str) {
    sort(str.begin(), str.end());
    do {
        cout << str << endl;
    } while (next_permutation(str.begin(), str.end()));
}

int main() {
    // string s {"cba"};
    string s {"yqrbgjdwtcaxzsnifvhmou"};

    displayAllPermutations(s);

    return 0;
}
