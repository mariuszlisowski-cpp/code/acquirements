/*
File read sum
~ odczytuje plik, sprawdza poprawność odczytu i sumuje jego elementy
*/

#include <iostream>
#include <fstream>
#include <cstdlib> // metoda exit

using namespace std;

const int SIZE = 60;
int main() {
  char filename[SIZE];
  ifstream inFile;

  cout << "Nazwa pliku z danymi?" << endl << ": ";
  cin.getline(filename, SIZE);
  inFile.open(filename);
  // czy udało się otworzyć plik?
  if (!inFile.is_open()) {
    cout << "Otwarcie pliku " << filename << " nie powiodło się!" << endl;
    exit(EXIT_FAILURE);
  }

  double  value, sum = 0.0;
  int count = 0; // liczba elementów do odczytu

  // odczyt pliku
  inFile >> value; // pobierz pierwszą wartość
  while (inFile.good()) {
    ++count;
    sum += value;
    inFile >> value;
  }

  /* odczyt w krótszej wersji
  while (inFile >> value) {
    ++count;
    sum += value;
  }
  */

  // wykrywanie zdarzeń
  if (inFile.eof())
    cout << "Koniec pliku!" << endl;
  else if (inFile.fail())
    cout << "Wczytywanie danych przerwane - błąd odczytu!" << endl;
  else
    cout << "Wczytywanie danych przerwane z nieznanej przyczyny!" << endl;

  // wyświetlanie danych
  if (count == 0)
    cout << "Niczego nie wczytano!" << endl;
  else {
    cout << "Wczytano " << count << " elementów" << endl;
    cout << "Ich suma wynosi " << sum << endl;
  }

  inFile.close();

  return 0;
}

/*
Przydład danych w pliku:

44 64.4 22 4.3 566 22
3 45 543 45.2 4.2 45
2.55
*/
