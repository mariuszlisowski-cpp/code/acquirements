#include <functional>
#include <iostream>

void f(int& n1, int& n2, const int& n3) {
    std::cout << "> in function:      " << n1 << ' ' << n2 << ' ' << n3 << '\n';
    ++n1;                                                                           // increments the copy of n1 
                                                                                    // stored in the function object
    n2 = 77;                                                                        // changes the main()'s n2
    // ++n3;                                                                        // ERROR: const
}

int main() {
    /* bound with current values */
    int n1 = 1;
    int n2 = 2;
    int n3 = 3;
    std::function<void()> bound_f = std::bind(f, n1, std::ref(n2), std::cref(n3));  // n1 by copy
                                                                                    // n2 by reference
                                                                                    // n3 by const reference
    /* values changed */    
    n1 = 10;
    n2 = 11;
    n3 = 12;
    
    std::cout << "> before function: " << n1 << ' ' << n2 << ' ' << n3 << '\n';
    bound_f();                                                                      // 
    std::cout << "> after function:  " << n1 << ' ' << n2 << ' ' << n3 << '\n';

    return 0;
}
