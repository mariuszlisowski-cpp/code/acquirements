#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

void fill_vector_randomly(std::vector<int>& vec, int min, int max) {
    std::mt19937 mt(std::random_device{}());
    std::uniform_int_distribution uid(min, max);
    std::generate(begin(vec), end(vec),
                  std::bind(uid, std::ref(mt)));
}

int main() {
    constexpr size_t size = 49;
    std::vector<int> v(size);

    fill_vector_randomly(v, 1, 9);                                          // range [a, b] (inclusive)

    /* print */
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
