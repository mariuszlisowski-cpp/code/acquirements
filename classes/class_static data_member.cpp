// class static data member
// #static

#include <iostream>

class Robot {
private:
   int id;
public:
   Robot(int n):id(n) {
      robotsNo++;
   }
   ~Robot() {
      robotsNo--;
   }
   int getId() {
      return id;
   }
   // static data member
   static int robotsNo; // no memory assigned yet
};

int Robot::robotsNo = 0; // memory assigned (on stack)

void showRobotsNo();

using namespace std;

int main() {
   const int maxRobots = 3;
   Robot* android[maxRobots];

   for (int i = 0; i < maxRobots; i++) {
      android[i] = new Robot(i+1);
      cout << "Robot no " << android[i]->getId() << " created" << endl;
   }
   showRobotsNo();

   cout << "Robot no " << android[0]->getId() << " deleted!" << endl;
   delete android[0];
   showRobotsNo();


  return 0;
}

void showRobotsNo() {
   cout << "Robots no: " << Robot::robotsNo << endl;
}
