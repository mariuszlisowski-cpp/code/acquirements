// #include <array>
#include <iostream>
#include <iterator>
#include <numeric>  // std::iota
#include <vector>

template
    < typename T
    , template<typename ELEM, typename ALLOC=std::allocator<ELEM> > class Container>
std::ostream& operator<< (std::ostream& out, const Container<T>& container) {
    typename Container<T>::const_iterator it = container.begin();

    out << "[";
    while(it != container.end()) {
        out << " " << *it++;
    }
    out << " ]";

    return out;
}

int main() {
    std::vector<int> vec(5);
    std::iota(vec.begin(), vec.end(), 1);

    // use overloaded output operator
    std::cout << vec << std::endl;

    return 0;
}
