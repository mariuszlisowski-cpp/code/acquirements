// decltype comparator since c++20

#include <iostream>
#include <set>

struct Point
{
    int x;
    int y;
};

/* custom comparator */
using Points = std::set<Point, decltype([](const Point& lhs, const Point& rhs) {
                                            return lhs.x > rhs.x;
                                        })>;

int main() {
    Points points{{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}};

    for (const auto& point : points) {
        std::cout << point.x << std::endl;
    }

    return 0;
}
