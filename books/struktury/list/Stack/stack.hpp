// Top -> node -> node
// Head -> node -> node
// ADD Head: O(1)
// REMOVE Head: O(1)

#ifndef STACK_H
#define STACK_H

class Stack {
private:
    int count;
    Node * Top; // Head
public:
    Stack();

    bool isEmpty();
    int top();
    void push(int);
    void pop();
    Node * getTop();
};

Stack::Stack():count(0), Top(nullptr) {}

int Stack::top() {
    return Top->value;
}

bool Stack::isEmpty() {
    return count <= 0;
}

void Stack::push(int val) {
    Node * node = new Node(val);
    node->Next = Top;
    Top = node;
    count++;
}

void Stack::pop() {
    if (isEmpty())
        return;
    Node * node = Top;
    Top = node->Next;
    delete node;
    count--;
}

Node * Stack::getTop() {
    if (!isEmpty())
        return Top;
    return nullptr;
}

#endif
