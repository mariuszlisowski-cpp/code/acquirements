#include <iostream>
#include <vector>

template<typename X,
         typename Y,
         template<typename, typename> class Pair,
         template<typename...> class Vector>
void print_vector_of_pairs(Vector<Pair<X, Y>> pairs) {
	for (Pair<X, Y> pair : pairs) {
		std::cout << pair.first << " : " << pair.second << std::endl;
	}

}


int main() {
	std::vector<std::pair<int, char>> pairs {
		{1, 'a'}, {2, 'b'}, {3, 'c'}
	};

	print_vector_of_pairs(pairs);
	
	return 0;
}
