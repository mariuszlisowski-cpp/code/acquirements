template <typename T, typename ... Args>
bool within(T min, T max, Args&& ... args) {
    return ((min <= args && args <= max) && ...);
    /* the above unfolds to:
    return ((min <= 1 && args <= 1) &&              // false
            (min <= 11 && args <= 11) &&
            (min <= 20 && args <= 20)); */
}

int main() {
    within(10, 20, 1, 11, 20);                      // false as 1 is out of range
    within(10, 20, 10, 11, 20);                     // true 
}
