#include <iostream>
#include <limits>
#include <map>
#include <string>

std::string highest_value(const std::map<std::string, int>& scores) {
    std::string winner;
    int max{std::numeric_limits<int>::min()};
    for (const auto& point : scores) {
        if (point.second > max) {
            max = point.second;
            winner = point.first;
        }
    }

    return winner;
}

int main() {
	std::map<std::string, int> scores{
		{"ab", 6},
		{"cd", 7},
		{"ef", 4},
		{"gh", 9},							// highest value
		{"ij", 2}
	};

	std::cout << "> key with the highest value: " << highest_value(scores) << std::endl;

	return 0;
}
