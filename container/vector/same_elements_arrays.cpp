// same elements in arrays
// #sort #set_intersection #back_inserter

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

vector<string> matchingStrings(vector<string>, vector<string>);
void showArray(vector<string>);

int main() {
   cout << "First array..." << endl;
   vector<string> inputs { "aba", "baba", "aba", "xzxb" };
   showArray(inputs);

   cout << "Second array..." << endl;
   vector<string> queries { "aba", "xzxb", "ab" };
   showArray(queries);

   cout << "Same elements in both arrays..." << endl;
   vector<string> sameElements = matchingStrings(inputs, queries);
   showArray(sameElements);

   return 0;
}

vector<string> matchingStrings(vector<string> strings, vector<string> queries) {
   sort(strings.begin(), strings.end());
   sort(queries.begin(), queries.end());

   vector<string> results;
   set_intersection(strings.begin(), strings.end(),
                    queries.begin(), queries.end(),
                    back_inserter(results));
   //
   return results;
}

void showArray(vector<string> vec) {
   for (auto x : vec)
      cout << x << " ";
   cout << endl;
}
