/*  Representation: edge list
    O(V + E) space,  V - vertices, E - edges                    // OK
    O(V * V) time                                               // COSTLY
    max edges: 
      V*(V-1)     directed graph (vertex A is connected to B
                                  or B is connected to A)       // almost square
      V*(V-1) / 2 undirected graph (vertex A is connected to B 
                                    and vice versa)
 */
#include <forward_list>
#include <iostream>
#include <string>

struct Edge {
    Edge(const std::string& start, const std::string& end)
        : start_vertex_(start), end_vertex_(end) {}

    std::string start_vertex_;
    std::string end_vertex_;
    int weight_;                                                // optional for weighted graph
                                                                // e.g. distance
};

int main() {
    /* 8 vertexes, 10 edges, no weights
         A - B
        / |  | \
        C D  E F
        |  \/ /
        G - H
    */
    std::forward_list<std::string> vertex_list{                 // 8 vertices (letters)
        "A", "B", "C", "D", "E", "F", "G", "H"
    };
    std::forward_list<Edge> edge_list{                          // 10 edges (connecting lines)
        Edge("A", "B"),
        Edge("A", "C"),
        Edge("A", "D"),
        Edge("B", "E"),
        Edge("B", "F"),
        Edge("C", "G"),
        Edge("D", "H"),
        Edge("E", "H"),
        Edge("F", "H"),
        Edge("G", "H"),
    };

    return 0;
}
