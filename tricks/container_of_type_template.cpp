#include <iostream>
#include <iterator>
#include <list>
#include <numeric>

template<template <typename, typename> class Container, 
         typename T,
         typename Allocator = std::allocator<T> >
void print(const Container<T, Allocator>& constainer) {
    std::copy(begin(constainer), end(constainer),
              std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::list<char> l(10);
    std::iota(l.begin(), l.end(), 'a');

    print(l);

}
