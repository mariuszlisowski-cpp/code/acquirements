#include "hr.h"

HR::HR(std::vector<std::unique_ptr<Employee>>&& employees) : employees(std::move(employees)) {}

void HR::processEmployee(Function func) {
    std::for_each(employees.begin(),
                  employees.end(),
                  [&func](auto& employee){
                      func(employee.get());
                  });
}
