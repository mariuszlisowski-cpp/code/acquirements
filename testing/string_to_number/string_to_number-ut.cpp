#include <gtest/gtest.h>

#include "string_to_number.hpp"

TEST(AtoiTest, ConvertPositiveInteger) {
    // given
    Solution solution;
    std::string given = "42";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeInteger) {
    // given
    Solution solution;
    std::string given = "-42";
    std::string expected = "-42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceBefore) {
    // given
    Solution solution;
    std::string given = " 42";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceAfter) {
    // given
    Solution solution;
    std::string given = "42 ";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceBeforeAndAfter) {
    // given
    Solution solution;
    std::string given = " 42 ";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithSpaceBefore) {
    // given
    Solution solution;
    std::string given = " -42";
    std::string expected = "-42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithSpaceAfter) {
    // given
    Solution solution;
    std::string given = "-42 ";
    std::string expected = "-42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAndSpaceBefore) {
    // given
    Solution solution;
    std::string given = "- 42";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAndSpaceBeforeAndMinusAfter) {
    // given
    Solution solution;
    std::string given = "- 42-";
    std::string expected = "42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAfter) {
    // given
    Solution solution;
    std::string given = "4-2";
    std::string expected = "4";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithMinusAfter) {
    // given
    Solution solution;
    std::string given = "-4-2";
    std::string expected = "-4";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithLetterBeforeAndAfter) {
    // given
    Solution solution;
    std::string given = "a-42b";
    std::string expected = "-42";
    // when
    std::string number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

