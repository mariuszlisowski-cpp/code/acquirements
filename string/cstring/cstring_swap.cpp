#include <iostream>
#include <cstring>

int main() {
    char str1[10] {"Alicja"};
    char str2[10] {"Natalia"};
    size_t i{};
    while (i < sizeof(str1)) {
        char tmp = *(str1+i);
        *(str1+i) = *(str2+i);
        *(str2+i) = tmp;
        ++i;
    }

    std::cout << str1 << std::endl;
    std::cout << str2 << std::endl;

    return 0;
}
