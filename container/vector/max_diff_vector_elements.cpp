// maximum differece between elements of vector
// #nested_iterator_loop

#include <vector>
#include <iostream>

using namespace std;

class Difference {
private:
  vector<int> elements;
public:
  int maximumDifference;

  Difference(vector<int> v):elements(v) {}

  void computeDifference() {
    int absolute, max = 0;
    vector<int>::const_iterator it;
    vector<int>::const_iterator itt;
    for (it = elements.begin(); it != elements.end(); it++) {
      absolute = 0;
      for (itt = elements.begin(); itt != elements.end(); itt++) {
        absolute = abs(*it - *itt);
        if (absolute > max)
        max = absolute;
        cout << *it << " - " << *itt << " = " << absolute << endl;
      }
    }
    maximumDifference = max;
  }
};

int main() {
  vector<int> a = { 1, 2, 5 };

  Difference d(a);
  d.computeDifference();

  cout << "Max: " << d.maximumDifference << endl;

  return 0;
}
