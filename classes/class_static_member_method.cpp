// class static member method
// #static #private

#include <iostream>

class Robot {
private:
   int id;
   // static private data member
   static int robotsNo;
public:
   Robot(int n):id(n) {
      robotsNo++;
   }
   ~Robot() {
      robotsNo--;
   }
   int getId() {
      return id;
   }
   // static member method
   static int getRobotsNo() {
      return robotsNo;
   }
};

int Robot::robotsNo = 0; // memory assigned (on stack)

void showRobotsNo();

using namespace std;

int main() {
   const int maxRobots = 3;
   Robot* android[maxRobots];

   for (int i = 0; i < maxRobots; i++) {
      android[i] = new Robot(i+1);
      cout << "Robot no " << android[i]->getId() << " created" << endl;
   }
   showRobotsNo();

   cout << "Robot no " << android[0]->getId() << " deleted!" << endl;
   delete android[0];
   showRobotsNo();


  return 0;
}

void showRobotsNo() {
   cout << "Robots no: " << Robot::getRobotsNo() << endl;
}
