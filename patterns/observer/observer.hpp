#pragma once

#include <memory>

class Observer {
public:
    virtual ~Observer() = default;
    virtual int getID() = 0;
    virtual void update() = 0;
};

using ObserverPtr = std::shared_ptr<Observer>;
