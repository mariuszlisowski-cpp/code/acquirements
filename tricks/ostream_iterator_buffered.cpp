#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> v{ 97, 98, 99 };

    /* regular ostream iterator */
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<char>(std::cout, " "));             // accepts other types, delimiter
    std::copy(v.begin(), v.end(),
              std::ostream_iterator<int>(std::cout, " "));              // saa

    /* buffered ostream iterator */
    std::copy(v.begin(), v.end(),
              std::ostreambuf_iterator<char>(std::cout));               // accepts 'char' only, no delimiter

    return 0;
}
