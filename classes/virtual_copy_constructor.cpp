// virtual copy copy_constructor
// #copy_constructor #virtual #clone()

#include <iostream>

using namespace std;

class Computer {
private:
   int memory;
public:
   // default constructor (inline)
   Computer():memory(1) {
      cout << ": Computer constructor" << endl;
   }
   // virtual destructor (inline)
   virtual ~Computer() {
      cout << "~ Computer destructor" << endl;
   }
   // copy contructor
   Computer(const Computer&);

   // getter (inline)
   int getMemory() const {
      return memory;
   }

   // virtual methods (inline)
   virtual void booting() const {
      cout << "Computer's booting..." << endl;
   }
   virtual Computer* clone() {
      return new Computer(*this);
   }

};

Computer::Computer(const Computer& rhs):memory(rhs.getMemory()) {
   cout << ": Computer copy constructor" << endl;
}

class Desktop : public Computer {
public:
   Desktop() {
      cout << ": Desktop constructor" << endl;
   }
   virtual ~Desktop() {
      cout << "~ Desktop destructor" << endl;
   }
   // copy contructor
   Desktop(const Desktop&);

   virtual void booting() const {
      cout << "Desktop's booting..." << endl;
   }
   virtual Computer* clone() {
      return new Desktop(*this);
   }
};

Desktop::Desktop(const Desktop& rhs):Computer(rhs){
   cout << ": Desktop copy constructor" << endl;
}

class Laptop : public Computer {
public:
   Laptop() {
      cout << ": Laptop constructor" << endl;
   }
   virtual ~Laptop() {
      cout << "~ Laptop destructor" << endl;
   }
   // copy contructor
   Laptop(const Laptop&);

   virtual void booting() const {
      cout << "Laptop's booting..." << endl;
   }
   virtual Computer* clone() {
      return new Laptop(*this);
   }
};

Laptop::Laptop(const Laptop& rhs):Computer(rhs) {
   cout << ": Laptop copy constructor" << endl;
}

void divider();

int main() {
   Computer* ptrC;
   ptrC = new Computer;
   divider();

   Computer* ptrD;
   ptrD = new Desktop;
   divider();

   Computer* ptrL;
   ptrL = new Laptop;
   divider();

   ptrC->booting();
   Computer* ptrCclone = ptrC->clone();
   ptrCclone->booting();
   divider();

   ptrD->booting();
   Computer* ptrDclone = ptrD->clone();
   ptrDclone->booting();
   divider();

   ptrL->booting();
   Computer* ptrLclone = ptrL->clone();
   ptrLclone->booting();
   divider();

  return 0;
}

void divider() {
   cout << string(30, '-') << endl;
}
