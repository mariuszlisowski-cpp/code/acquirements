#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

class Solution {
public:
    std::string myAtoi(std::string s) {
        std::string number{};
        bool isNegative {false};
        bool isDigit {false};
        bool isMinus {false};
        for (char& ch : s) {
            if (std::isdigit(ch)) {
                isDigit = true;
            } else {
                if (!isDigit) {
                    isDigit = false;
                    isMinus = false;
                } else {
                    break;
                }
            }
            if (ch == '-') {
                isMinus = true;
            } 
            // ---------------------
            if (isDigit) {
                number.append({ch});
            } else if (isMinus) {
                isNegative = true;
            } else {
                isNegative = false;
            }
        }        
        
        if (isNegative) {
            number.insert(0, "-");
        }

        return number;
    }

};
