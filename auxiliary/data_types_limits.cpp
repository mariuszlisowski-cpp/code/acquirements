// data types limits

#include <iostream>
#include <limits>

using namespace std;

int main() {
   // short
   short smin = numeric_limits<short>::min();
   short smax = numeric_limits<short>::max();
   cout << "Short:\t\tMIN " << smin << "\t\t | MAX " << smax << endl;

   // integer
   int imin = numeric_limits<int>::min();
   int imax = numeric_limits<int>::max();
   cout << "Integer:\tMIN " << imin << "\t\t | MAX " << imax << endl;

   // long
   long lmin = numeric_limits<long>::min();
   long lmax = numeric_limits<long>::max();
   cout << "Long:\t\tMIN " << lmin << " | MAX " << lmax << endl;

   // long long
   long long llmin = numeric_limits<long long>::min();
   long long llmax = numeric_limits<long long>::max();
   cout << "Long long:\tMIN " << llmin << " | MAX " << llmax << endl;

   // float
   float fmin = numeric_limits<float>::min();
   float fmax = numeric_limits<float>::max();
   cout << "Float:\t\tMIN " << fmin << "\t\t | MAX " << fmax << endl;

   // double
   double dmin = numeric_limits<double>::min();
   double dmax = numeric_limits<double>::max();
   cout << "Double:\t\tMIN " << dmin << "\t | MAX " << dmax << endl;

  return 0;
}
