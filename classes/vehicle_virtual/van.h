// derived class header

#pragma once

using std::string;

class Van : public Vehicle {
protected:
  int loadCapacity;
public:
  // constructors
  Van();
  Van(const int); // derived class parameter
  Van(const string, const int, const int); // base & derived class parameters

  // virtual destructor
  virtual ~Van(); // must be virtual

  // overriden function
  virtual void display() const; // good practice to make it virtual as well

  // overriden pure function
  virtual void drive();
};
