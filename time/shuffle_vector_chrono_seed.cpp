#include <algorithm>
#include <chrono>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

template<typename CONTAINER, typename T>
void print(CONTAINER c) {
    std::copy(begin(c), end(c),
              std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    constexpr int DISTR_MIN{ 10 };
    constexpr int DISTR_MAX{ 99 };
    constexpr size_t CONTAINER_SIZE{ 10 };
    constexpr size_t LOOPS{ 5 };

    auto seed{ std::chrono::system_clock().now().time_since_epoch().count() };
    std::mt19937 mt(seed);
    std::uniform_int_distribution distr(DISTR_MIN, DISTR_MAX);

    std::vector<int> v(CONTAINER_SIZE);
    std::generate(begin(v), end(v),
                  [&]() {
                      return distr(mt);
                  });

    print<decltype(v), decltype(v)::value_type>(v);

    /* shuffling few times */
    for (size_t i = 0; i < LOOPS; i++) {
        std::shuffle(begin(v), end(v), mt);
        print<decltype(v), decltype(v)::value_type>(v);
    }

    return 0;
}
