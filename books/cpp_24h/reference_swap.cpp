// Zamaian zmiennych przez referencję
// Przykład:
// int value;
// int &rValue = value;

#include <iostream>

using namespace std;

void swap(int &x, int &y); // prototyp funkcji zawiadamia o sposobie jej użycia (zastosowaniu referencji jako argumentów poprzez użycie & 'ampersand')

int main()
{
  int x = 5;
  int y = 10;
  cout << "\nPrzed zamianą:\t x=" << x << ", y=" << y;
  swap(x, y); // funkcja przekazuje wartości zmiennych (już nie adresy)
  cout << "\nPo zamianie:\t x=" << x << ", y=" << y;
  cout << endl << endl;
  return 0;
}

void swap(int &rx,int &ry) // przekazanie argumentów poprzez referencję
{
  cout << "\nZamiana...";
  int temp;
  temp = rx; // przypisanie wartości przez wyłuskanie
  rx = ry;  // j.w.
  ry = temp; // j.w.
}
