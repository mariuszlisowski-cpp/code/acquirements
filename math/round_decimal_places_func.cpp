/*
Round to n decimal places;
~ maximum 5 (?)
*/

#include <iostream>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

double roundToPlaces(double, int);
double truncateDouble(double, int);

int main() {
  int n = 8; // ?
  double pi = 3.141592653589;
  cout << roundToPlaces(pi, n) << endl;
  cout << truncateDouble(pi, n) << endl;

  return 0;
}

double roundToPlaces(double value, int to) {
    double places = pow(10.0, to);
    return round(value * places) / places;
}

double truncateDouble(double val, int digits) {
    double temp = 0.0;
    temp = (int) (val * pow(10, digits));
    temp /= pow(10, digits);
    return temp;
}
