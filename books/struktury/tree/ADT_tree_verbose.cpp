// abstract data type (tree)

#include <iostream>

using namespace std;

class TreeNode {
public:
    int Key;
    TreeNode * Left;
    TreeNode * Right;
    TreeNode(int); // constructor declaration
};

// constructor
TreeNode::TreeNode(int key = 0) {
    Key = key;
    Left = nullptr;
    Right = nullptr;
}

// create new tree node	function
TreeNode * NewTreeNode(int key) {
    TreeNode * node = new TreeNode();
    node->Key = key;
    node->Left = nullptr;
    node->Right = nullptr;
    return node;
}

void printTreeInOrder(TreeNode * node) {
    // verbose
    cout << "----------------------------" << endl;
    static int i {1}, j {1};
    
    if (!node) {
        cout << "Empty node - RETURN" << endl;
        return;
    }

    // verbose
    cout << "Recursion A" << i++ << " ENTER " << "at key " << node->Key << endl;
    // get left child
    printTreeInOrder(node->Left);  // RECURSION A
    // verbose
    cout << "Recursion A" << --i << " EXIT  " << "at key " << node->Key << endl;
    
    std::cout << "-> " << node->Key << endl; // print the value
    
    // verbose
    cout << "Recursion B" << j++ << " ENTER " << "at key " << node->Key << endl;
    // get right child
    printTreeInOrder(node->Right);  // RECURSION B
    // verbose
    cout << "Recursion B" << --j << " EXIT  " << "at key " << node->Key << endl;
}

int main() {
    // add root (using constructor)
    // TreeNode * root = NewTreeNode(1);
    TreeNode * root = new TreeNode(1);
    //   1
    ///////////////////////////////////////
    // add children to '1' (using function)
    root->Left = NewTreeNode(2);
    root->Right = NewTreeNode(3);
    //   1
    //  / \
    // 2   3
    ///////////////////////////////////////

    printTreeInOrder(root);

    return 0;
}