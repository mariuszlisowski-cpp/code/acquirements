#include <iostream>

int main() {
    char cstr[] {"Programowanie w jezyku C++"};
    size_t length = sizeof(cstr) - 1;
    for (size_t i = 0; i < length / 2; ++i) {
        char tmp = cstr[i];
        cstr[i] = cstr[length-i-1];
        cstr[length-i-1] = tmp;
    }
    std::cout << cstr << std::endl;
}
