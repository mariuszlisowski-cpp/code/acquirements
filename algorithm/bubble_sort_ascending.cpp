// ascending bubble sort
// complexity: O(n*n) [slow]

#include <iostream>
#include <vector>

using namespace std;

int bubbleSortAscending(vector<int>&, const int &);

int main() {
  int n;
  cout << "Array size?" << endl << ": ";
  cin >> n;
  vector<int> a(n); // initialized size
  for (int i = 0; i < n; i++) {
    cout << i + 1 << " value: ";
    cin >> a[i]; // no need to use push_back method as the size is fixed
  }

  int numSwaps = bubbleSortAscending(a, n);

  cout << "Array's been sorted in " << numSwaps << " swaps" << endl;
  cout << "First Element:\t" << a[0] << endl;
  cout << "Last Element:\t" << a[a.size() - 1] << endl;

  return 0;
}

int bubbleSortAscending(vector<int>& arr, const int &size) {
  int numberOfSwaps = 0;
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size - 1; j++) {
      // swap adjacent elements if they are in decreasing order
      if (arr[j] > arr[j + 1]) {
        swap(arr[j], arr[j + 1]);
        numberOfSwaps++;
      }
    }
    // if no elements were swapped during a traversal, array is sorted
    if (numberOfSwaps == 0)
      break;
  }
  return numberOfSwaps;
}
