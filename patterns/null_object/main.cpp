// design pattern NULL
// ~ unfinished - TO BE FIXED
// ~ bug in customerList.cpp - addFoundCustomerToFoundList() method

#include <iostream>
#include <memory>

#include "customerList.hpp"
#include "customer.hpp"

using CustomerPtr = std::shared_ptr<Customer>;

int main() {
    CustomerService manager;
    CustomerList finder;

    CustomerPtr customer1 = std::make_shared<Customer>("Alan", "Smith");
    CustomerPtr customer2 = std::make_shared<Customer>("Ian", "Nobody");

    manager.addCustomerToRegister(customer1);
    manager.addCustomerToRegister(customer2);
    // manager.addCustomerToRegister(customer2);
    manager.displayCustomerRegister();
    
    std::string surnameToFind{ "Smith" };
    CustomerList foundCustomersList = manager.findCustomerBySurname(surnameToFind);
    finder.displayFoundList();

    return 0;
}
