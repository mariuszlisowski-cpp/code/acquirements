// BST level order
// #BST #traversal #queue

#include <iostream>
#include <queue>

using namespace std;

class Node {
public:
   int data;
   Node *left;
   Node *right;
   Node(int d) {
      data = d;
      left = NULL;
      right = NULL;
   }
};

class BinarySearchTree {
public:
   Node* insert(Node* root, int data) {
      if (root == NULL) {
         return new Node(data);
      }
      else {
         Node* cur;
         if (data <= root->data) {
         cur = insert(root->left, data);
            root->left = cur;
         }
         else {
            cur = insert(root->right, data);
            root->right = cur;
         }
         return root;
      }
   }

   void levelOrder(Node * root){
      if (root == NULL) return;
      queue<Node*> pQ; // queue of pointers
      pQ.push(root); // first pointer
      // traverse until the queue is empty
      while (!pQ.empty()) {
         Node* current = pQ.front();
         cout << current->data << endl;
         if (current->left != NULL)
            pQ.push(current->left); // left child to the queue
         if (current->right != NULL)
            pQ.push(current->right); // right child to the queue
         pQ.pop(); // removes fornt from the queue
      }
   }
};

int main() {
   BinarySearchTree myTree;
   Node* root = NULL;
   int data;

   int arr[] = { 3, 5, 4, 7, 2, 1 };
   size_t arrSize = sizeof(arr) / sizeof(*arr);

   for (int i = 0; i < arrSize; i++)
      root = myTree.insert(root, arr[i]);

   myTree.levelOrder(root);

   return 0;
}
