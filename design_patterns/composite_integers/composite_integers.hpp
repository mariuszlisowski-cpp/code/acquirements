# pragma once

#include <numeric>
#include <vector>

struct ContainsIntegers                                                     // interface
{
    virtual ~ContainsIntegers() {}
    virtual int sum() = 0;                                                  // pure virtual method
};

struct SingleValue : ContainsIntegers
{
    SingleValue();
    explicit SingleValue(const int value);

    int sum() override;

    int value{0};
};

struct ManyValues : std::vector<int>, ContainsIntegers
{
    ManyValues();

    explicit ManyValues(const int value);
        
    void add(const int value);
    int sum() override;
};
