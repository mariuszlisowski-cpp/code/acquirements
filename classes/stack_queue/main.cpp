// stack & queue class implementation
// copile with 'g++ --std=c++11 *.cpp'

#include <iostream>
#include "queue.h"
#include "stack.h"
#include "queue_element.h"
#include "stack_element.h"

using namespace std;

void divider();

int main() {
  // queue
  Queue queueObject;
  queueObject.enqueueCharacter('A');
  queueObject.enqueueCharacter('B');
  queueObject.enqueueCharacter('C');

  cout << "Queue:" << endl;
  queueObject.showQueue();
  divider();

  cout << "~ dequeued: " << queueObject.dequeueCharacter() << endl;
  divider();

  cout << "Queue:" << endl;
  queueObject.showQueue();
  divider();

  // stack
  Stack stackObject;
  stackObject.pushCharacter('D');
  stackObject.pushCharacter('E');
  stackObject.pushCharacter('F');

  cout << "Stack:" << endl;
  stackObject.showStack();
  divider();

  cout << "~ popped: " << stackObject.popCharacter() << endl;
  divider();

  cout << "Stack:" << endl;
  stackObject.showStack();
  divider();

  return 0;
}

void divider() {
  cout << string(14, '-') << endl;
}
