// lamda call with parameters

#include <iostream>

int main() {
    std::cout <<
        [](const double mulitplicand, const double mulltiplier) {
            return mulitplicand * mulltiplier;
        }(50.0, 10.0) << std::endl;


    return 0;
}
