//

#include <iostream>
#include "node.hpp"
#include "stack.hpp"

using namespace std;

void showTop(Stack &st) {
    if (!st.isEmpty())
        cout << st.top() << endl;
    else
        cout << "Stack empty!" << endl;
}

void showStack(Stack &st) {
    Node * node = st.getTop();
    while (node) {
        cout << node->value << endl;
        node = node->Next;
    }
    cout << "NULL" << endl;
}

void deleteStack(Stack &st) {
    while (!st.isEmpty())
        st.pop();
}

int main() {
    Stack stack;
    showTop(stack);

    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);
    stack.push(50);
    stack.pop();
    showStack(stack);

    deleteStack(stack);

    return 0;
}
