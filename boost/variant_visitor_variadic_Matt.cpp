/* https://bitbashing.io/std-visit.html */

#include "boost/variant/detail/apply_visitor_unary.hpp"
#include <boost/variant.hpp>
#include <iostream>
#include <string>

template <class... Fs>
struct overload;

template <class F0, class... Frest>
struct overload<F0, Frest...> : F0, overload<Frest...>
{
    overload(F0 f0, Frest... rest) : F0(f0), overload<Frest...>(rest...) {}

    using F0::operator();
    using overload<Frest...>::operator();
};

template <class F0>
struct overload<F0> : F0
{
    overload(F0 f0) : F0(f0) {}

    using F0::operator();
};

template <class... Fs>
auto make_visitor(Fs... fs)
{
    return overload<Fs...>(fs...);
}

int main() {
    boost::variant<int, std::string> v{"text"};

    auto visitor = make_visitor(
        [&](const std::string& str) { printf("string: %s\n", str.c_str()); },
        [&](const int value) { printf("integer: %d\n", value); }
    );
    
    boost::apply_visitor(visitor, v);

    v = 42;
    boost::apply_visitor(visitor, v);

    return 0;
}
