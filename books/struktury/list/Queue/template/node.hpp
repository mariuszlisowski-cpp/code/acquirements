#pragma once

template<class T>
class Node {
public:
    T value;
    Node<T> * Next;
    Node(T);
};

template<class T>
Node<T>::Node(T val):value(val), Next(nullptr) {}
