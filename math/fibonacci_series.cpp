#include <iostream>

void printFiboRecur(int n) {
    static int first = 0, 
               second = 1, 
               third;
    if (n > 0) {
        third = first + second;
        first = second;
        second = third;
        std::cout << third << ' ';
        printFiboRecur(n - 1);
    }
}

void printFiboSeriesOf(int n) {
    int first{};
    int second{1};
    int third;
    for (int i = 0; i < n; ++i) {
        std::cout << first << ' ';
        third = first + second;
        first = second;
        second = third;
    }
    std::cout << std::endl;
}

void printFiboTillMaxOf(int n) {
    int first{};
    int second{1};
    int third{first + second};
    std::cout << first << ' ' << second << ' ';
    while (third <= n) {
        std::cout << third << ' ';
        first = second;
        second = third;
        third = first + second;
    }
    std::cout << std::endl;
}

int main() {
    int maxNoSeries = 9;
    printFiboSeriesOf(maxNoSeries);
    printFiboTillMaxOf(33);

    std::cout << 0 << ' ' << 1 << ' ';    
    printFiboRecur(maxNoSeries - 2);
    return 0;
}
