#include <iostream>
#include <string>

template <typename ... Args>
auto sum(Args&& ... args) {
    return (0 + ... + args);                               // returns ZERO if no arguments
    /* the above unfolds to:
    return (0 + 2 + 3 + 5); */                             // call with arguments
}

template <typename ... Args>
auto product(Args ... args) {
    return (args * ... * 1);                               // returns ONE if no arguments
    /* the above folds to:
    return (2 * 2 * 2 * 1); */                             // call with arguments
}

int main() {
    std::cout << sum() << std::endl;                       // without arguments
    std::cout << sum(2, 3, 5) << std::endl;

    std::cout << product() << std::endl;
    std::cout << product(2, 2, 2) << std::endl;
}
