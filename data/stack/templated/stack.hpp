// Top -> node -> node
// Head -> node -> node
// ADD Head: O(1)
// REMOVE Head: O(1)

#ifndef STACK_H
#define STACK_H

template<class T>
class Stack {
private:
    int count;
    Node<T> * Top; // Head
public:
    Stack();

    bool isEmpty();
    T top();
    void push(T);
    void pop();
    Node<T> * getTop();
};

template<class T>
Stack<T>::Stack():count(0), Top(nullptr) {}

template<class T>
T Stack<T>::top() {
    return Top->value;
}

template<class T>
bool Stack<T>::isEmpty() {
    return count <= 0;
}

template<class T>
void Stack<T>::push(T val) {
    Node<T> * node = new Node<T>(val);
    node->Next = Top;
    Top = node;
    count++;
}

template<class T>
void Stack<T>::pop() {
    if (isEmpty())
        return;
    Node<T> * node = Top;
    Top = node->Next;
    delete node;
    count--;
}

template<class T>
Node<T> * Stack<T>::getTop() {
    if (!isEmpty())
        return Top;
    return nullptr;
}

#endif
