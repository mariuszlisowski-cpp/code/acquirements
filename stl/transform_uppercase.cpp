// transform uppercase
// #transform #toupper

#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    string m{"raisin"};
    transform(m.begin(), m.end(), m.begin(),
        [](unsigned char c){ return std::toupper(c); });

    cout << m << endl;
    return 0;
}
