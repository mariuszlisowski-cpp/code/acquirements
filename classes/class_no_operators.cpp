// class NO dot & arrow operators

#include <iostream>

using namespace std;

class Number {
private:
  double x;
public:
  // accessors (inline)
  void set(double y) { x = y; }
  double get() { return x; }

  // methods
  double add(double);
  double sub(double);
  double mul(double);
  double div(double);
  void info(const char*);
};

double Number::add(double y) {
  x += y;
  return x;
}

double Number::sub(double y) {
  x -= y;
  return x;
}

double Number::mul(double y) {
  x *= y;
  return x;
}

double Number::div(double y) {
  x /= y;
  return x;
}

void Number::info(const char* s) {
  cout << s << ":\t" << x << endl;
}

int main() {
  Number nr;
  nr.set(10);
  nr.info("set");
  nr.add(5);
  nr.info("add");
  nr.sub(6);
  nr.info("sub");

  Number no;
  no.set(20);
  no.info("set");
  no.div(2);
  no.info("divide");
  no.mul(5);
  no.info("multi");

  return 0;
}
