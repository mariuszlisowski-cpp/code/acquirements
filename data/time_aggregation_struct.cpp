// Time aggregation structure

#include <iostream>

struct travelTime {
  int hours;
  int mins;
};

travelTime sum(travelTime, travelTime);
void showTime(travelTime);

const int MinsPerHour = 60;

int main() {
  travelTime t1 = { 3, 30 }; // 3h30mins
  travelTime t2 = { 2, 33 }; // 2h33mins
  showTime(t1);
  showTime(t2);

  travelTime trip = sum(t1, t2);
  showTime(trip);

  return 0;
}

travelTime sum(travelTime aT1, travelTime aT2) {
  travelTime total;

  total.mins = (aT1.mins + aT2.mins) % MinsPerHour;
  total.hours = aT1.hours + aT2.hours + (aT1.mins + aT2.mins) / MinsPerHour;

  return total;
}

void showTime(travelTime aT) {
  std::cout << aT.hours << " hours " << aT.mins << " minutes" << std::endl;
}
