// iterator to pointer dereference
// ~ get class method via iterator

#include <iostream>
#include <set>
#include <iterator>

class Car {
public:
    Car(double price)
        : price_(price)
    {}

    double getPrice() const { return price_; }

private:
    double price_;
};

int main() {
    std::set<Car*> cars = { new Car{ 22999 }, new Car{10500} };

    // first element
    std::cout << (*cars.begin())->getPrice() << std::endl;
    
    // next element
    auto it = cars.begin();
    std::advance(it, 1);
    std::cout << (*it)->getPrice() << std::endl;
    // or..
    std::cout << (*++cars.begin())->getPrice() << std::endl;

    // all elements via iterator incrementation
    for (auto it = cars.begin(); it != cars.end(); ++it) {
        (*it)->getPrice();
    }

    // all elements via collection
    for (auto const& el : cars) {
        el->getPrice();
    }

    return 0;
}
