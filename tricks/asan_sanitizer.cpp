// g++ -std==c++17 -fsanitize=address

#include <iostream>

int main() {
    int* ptr{};
    [[maybe_unused]] auto value = *ptr;         // ERROR: nullptr dereference

    return 0;
}
