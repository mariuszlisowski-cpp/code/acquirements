
#ifndef NODE_H
#define NODE_H

class Node {
public:
    Node(int);

    int value;
    Node * Next;
};

Node::Node(int val):value(val), Next(nullptr) {}

#endif
