#include <iostream>
#include <vector>

int main() {
    // multiple variable loop of different type
    for (auto t = std::make_tuple(0, std::string("Hello world"), std::vector<int>{});
        std::get<0>(t) < 10;
        ++std::get<0>(t)) 
    {
        std::cout << std::get<1>(t) << std::endl; // cout Hello world
        std::get<2>(t).push_back(std::get<0>(t)); // add counter value to the vector
    }

    return 0;
}
