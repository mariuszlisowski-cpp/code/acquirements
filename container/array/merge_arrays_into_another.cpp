// merge arrays into another

#include <array>
#include <iostream>
#include <vector>

std::vector<int> mergeArraysLoopA(std::array<int, 3> arr1, std::array<int, 4> arr2) {
    std::vector<int> merged;
    size_t i{}, j{};
    while (i < arr1.size() || j < arr2.size()) {
        if (j == arr2.size() || (i < arr1.size() && arr1[i] < arr2[j])) {
            merged.push_back(arr1[i++]);
        } else {
            merged.push_back(arr2[j++]);
        }
    }
    return merged;
}

std::vector<int> mergeArraysLoopB(std::array<int, 3> arr1, std::array<int, 4> arr2) {
    std::vector<int> merged;
    size_t i{}, j{};
    while (i < arr1.size() && j < arr2.size()) {
        if (arr1[i] < arr2[j]) {
            merged.push_back(arr1[i++]);
        } else {
            merged.push_back(arr2[j++]);
        }
    }
    while (i < arr1.size()) {
            merged.push_back(arr1[i++]);
    }
    while (j < arr2.size()) {
            merged.push_back(arr2[j++]);
    }
    return merged;
}

void printVec(std::vector<int> merged) {
    for (const auto el : merged) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}
int main() {
    std::array<int, 3> arr1{ 1, 3, 5 };
    std::array<int, 4> arr2{ 2, 4, 6, 7 };

    std::vector<int> merged1 = mergeArraysLoopA(arr1, arr2);
    std::vector<int> merged2 = mergeArraysLoopB(arr1, arr2);

    printVec(merged1);
    printVec(merged2);

    return 0;
}