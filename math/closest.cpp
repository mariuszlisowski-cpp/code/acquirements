/* Program pobiera 5 liczb, oblicza ich średnią arytmetyczną i podaje liczbę/liczby najbliższe ich średniej */

#include <iostream>

using namespace std;

float liczby[5];

int main()
{
    //pobieranie 5 liczb w pętli
    cout << "Podaj 5 liczb... " << endl;
    float suma=0, srednia;
    for (int i=0; i<5; i++)
    {
        cout << i+1 << ": ";
        cin >> liczby[i];
        suma += liczby[i];
    }

    //obliczanie średniej
    srednia = suma / 5;

    //sortowanie od najmniejszej
    bool zamiana;
    int rozmiar=5;
    float t;
    do
    {
        zamiana = false;
        for (int i=0; i<rozmiar-1; i++)
        {
            if (liczby[i] > liczby[i+1])
            {
                t = liczby[i];
                liczby[i] = liczby[i+1];
                liczby[i+1] = t;
                zamiana = true;
            }

        }
        --rozmiar;
    }
    while (zamiana);

    //wyszukiwanie najbliższej liczby do średniej
    float bliska[2]; // mogą być dwie liczby dokładnie pośrodku
    int i=0;
    bool test=true;
    bool bliskie=false;
    do
    {
        if (srednia == liczby[i]) //gdy średnia jest jedną z podanych liczb
            bliska[0] = liczby[i];
        else
            if (srednia < liczby[i])
            {
                if ((liczby[i] - srednia) == (srednia - liczby[i-1])) //gdy średnia jest dokładnie pośrodku dwóch liczb
                {
                    bliska[0] = liczby[i-1];
                    bliska[1] = liczby[i];
                    bliskie = true; // wykryto dwie liczby dokładnie pośrodku, więc je wyświetl poprawnie
                }
                else
                    if ((liczby[i] - srednia) > (srednia - liczby[i-1])) //gdy średnia jest pośrodku dwóch liczb
                        bliska[0] = liczby[i-1];
                    else
                        bliska[0] = liczby[i];
                test = false; // wychodzimy z pętli
            }
        i++;
    }
    while (test);

    cout << endl << "Srednia arytmetyczna liczb:  " << srednia;
    if (bliskie)
        cout << endl << "Liczby najbliższe średniej:  " << bliska[0] << " i " << bliska[1];

    else
        cout << endl << "Liczba najbliższa średniej:  " << bliska[0];
    cout << endl;
    return 0;
}
