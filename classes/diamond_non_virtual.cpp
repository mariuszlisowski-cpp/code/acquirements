#include <iostream>

class A { int a; };
class B : public A { int b; };
class C : public A { int c; };
class D : public B, public C { int d; };

/*  | int a | int b | int a | int c | int d |
    ^-------^       ^-------^
        A               A
    ^---------------^---------------^
            B               C
    ^---------------------------------------^
                      D
 */
 
 int main() {
    

    return 0;
}
