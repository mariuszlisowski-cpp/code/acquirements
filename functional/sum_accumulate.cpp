// sum of vector elements
// #accumulate 

#include <iostream>
#include <numeric>
#include <vector>

int main() {
    std::vector<int> numbers{ 23, 64, -55, -43, 32, 9, 98 };

    std::cout << "Sum : "
              << std::accumulate(std::begin(numbers),
                                 std::end(numbers),
                                 0);

    return 0;
}
