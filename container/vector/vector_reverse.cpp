// vector reverse

#include <iostream>
#include <vector>

using namespace std;

template<class T>
void displayVectorReverse(const vector<T> &vec) {
    for (auto it = vec.rbegin(); it != vec.rend(); ++it)
        cout << *it << " | ";
    cout << endl;
}

int main() {
    vector<int> v {10, 20, 30, 40, 50};
    displayVectorReverse(v);

    return 0;
}
