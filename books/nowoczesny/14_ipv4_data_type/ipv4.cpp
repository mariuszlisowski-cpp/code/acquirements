#include "ipv4.hpp"

#include <exception>
#include <sstream>

ipv4::ipv4() : data_({0}) {}
ipv4::ipv4(const unsigned char a,
                     const unsigned char b,
                     const unsigned char c,
                     const unsigned char d) 
    : data_({a, b, c, d}) {}
constexpr ipv4::ipv4(unsigned long a)
    : data_{{static_cast<unsigned char>((a >> 24) & 0xFF),
            static_cast<unsigned char>((a >> 16) & 0xFF),
            static_cast<unsigned char>((a >> 8) & 0xFF),
            static_cast<unsigned char>(a & 0xFF)}} {}

std::ostream& operator<<(std::ostream& os, const ipv4& a) {
    os << static_cast<int>(a.data_[0]) << '.' 
         << static_cast<int>(a.data_[1]) << '.'
         << static_cast<int>(a.data_[2]) << '.'
         << static_cast<int>(a.data_[3]);
    
    return os;
}

ipv4::ipv4(const ipv4& other) noexcept : data_(other.data_) {}

ipv4& ipv4::operator=(const ipv4& other) {
    data_ = other.data_;

    return *this;
}

std::string ipv4::to_string() const {
    std::stringstream ss;
    ss<< *this;

    return ss.str();
}

constexpr unsigned long ipv4::to_ulong() const noexcept {
    return (static_cast<unsigned long>(data_[0]) << 24) |
           (static_cast<unsigned long>(data_[1]) << 16) |
           (static_cast<unsigned long>(data_[2]) << 8) | static_cast<unsigned long>(data_[3]);
}

std::istream& operator>>(std::istream& is, ipv4& a) {
    char d1, d2, d3;
    int b1, b2, b3, b4;
    is >> b1 >> d1 >> b2 >> d2 >> b3 >> d3 >> b4;
    if (d1 == '.' && d2 == '.' && d3 == '.') {
        a = ipv4(static_cast<unsigned char>(b1), 
                 static_cast<unsigned char>(b2),
                 static_cast<unsigned char>(b3),
                 static_cast<unsigned char>(b4));
    } else {
        is.setstate(std::ios_base::failbit);
        throw std::invalid_argument("input error");
    }
    
    return is;
}
