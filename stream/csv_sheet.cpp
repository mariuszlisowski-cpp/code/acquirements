/*
Arkusz CSV
Tworzy arkusz z losowymi liczbami
*/
#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

int main()
{
  srand(time(0));

  /* Ustawienia arkusza */
  short columns =5;
  short lines = 10;

  /* Zapis arkusza */
  ofstream f_write;
  f_write.open("data.csv");

  for (int line=0; line <lines; line++)
  {
    for (int column=0; column < columns; column++)
    {
      f_write << rand()%90 + 10; // generuje liczby 10-99
      if (column != columns-1) f_write << ';'; // dodaje średnik między liczbami
    }
    if (line != lines-1) f_write << '\n'; // łamaie linię znakiem 'enter'
  }
  f_write.close();

  /* Odczyt arkusza */
  ifstream f_read;
  f_read.open("data.csv");
  int output;
  for (int line=0; line <lines; line++)
  {
    for (int column=0; column <columns; column++)
    {
      f_read >> output; // odczytuje pierwszą liczbę
      cout << output;
      if (column != columns-1)
      {
        cout << "\t"; // rozdziel liczby tabulatorem
        f_read.ignore(1, ';'); // przesuń pozycję pliku za średnik
      }
    }
    if (line != lines-1) cout << endl;
  }
  f_read.close();

  cout << endl;
  return 0;
}
