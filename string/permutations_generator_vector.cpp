// permutations generator
// ~ a word or phrase formed by rearranging the letters of a different
// word or phrase, typically using all the original letters exactly once
// ~ no meaning

#include <iostream>
#include <algorithm>
#include <vector>

int factorial(int size) {
    if (size < 2) {
        return 1;
    }
    return factorial(size - 1) * size;
}

auto getAllPermutations(std::string str) {
    std::vector<std::string> words;
    words.reserve(factorial(str.size()));                               // max number of permutations reversed
    
    std::sort(str.begin(), str.end());                                  // start from the first permutation
    do {
        words.push_back(str);
    } while (next_permutation(str.begin(), str.end()));

    return words;
}

int main() {
    std::string s {"cba"};

    auto permutations = getAllPermutations(s);

    for (const auto& permutation : permutations) {
        std::cout << permutation << std::endl;
    }

    return 0;
}
