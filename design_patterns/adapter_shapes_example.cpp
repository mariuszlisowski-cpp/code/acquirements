#include <iostream>

struct Square {
    int side{};

    explicit Square(const int side) : side(side) {}
};

struct Rectangle {
    virtual int width() const = 0;
    virtual int height() const = 0;

    int area() const { return width() * height(); }
};

struct SquareToRectangleAdapter : Rectangle {
    SquareToRectangleAdapter(const Square& square) {
        width_ = square.side;        
        height_ = square.side;        
    }

    int width() const override {
        return width_;
    }

    int height() const override {
        return height_;
    }

    int width_{};
    int height_{};
};

int main() {
    Square square(5);
    SquareToRectangleAdapter adapter{square};
    
    std::cout << adapter.width() << std::endl;
    std::cout << adapter.height() << std::endl;

    std::cout << adapter.area() << std::endl;

    return 0;
}
