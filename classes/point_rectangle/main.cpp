
#include <iostream>
#include "point.h"
#include "rectangle.h"

using namespace std;

void pointInsideRectangle(const Point &point, const Rectangle &rect) {
    if (point.ox >= rect.ox && point.ox <= rect.ox + rect.height
        && point.oy >= rect.oy && point.oy <= rect.oy + rect.width)
        cout << "Punkt >" << point.name << "< NALEŻY do prostokąta >" << rect.name << "<" << endl;
    else
        cout << "Punkt >" << point.name << "< leży POZA prostokątem >" << rect.name << "<" << endl;
}

int main() {
    // default arguments
    Point pointA;
    Rectangle rectA;
    pointInsideRectangle(pointA, rectA);
    // set arguments
    Point pointB("Punkt", 2.1, 3);
    Rectangle rectB("Prostokąt", 0, 0, 2, 3);
    pointInsideRectangle(pointB, rectB);

    return 0;
}
