// duplicates in array
// ~ prints duplicates in array using unordered_set
// #unordered_set

#include <iostream>
#include <unordered_set>

using namespace std;

void printDuplicates(int[], int);

int main()
{
  int arr[] = {1, 5, 2, 1, 4, 3, 1, 7, 2, 8, 9, 5};
  int n = sizeof(arr) / sizeof(int);

  printDuplicates(arr, n);
  return 0;
}

void printDuplicates(int arr[], int n)
{
  // declaring unordered sets for checking and storing duplicates
  unordered_set<int> intSet;
  unordered_set<int> duplicate;

  for (int i = 0; i < n; i++)
  {
    // if element is not there then insert that
    if (intSet.find(arr[i]) == intSet.end()) // return past-the-end if not found
      intSet.insert(arr[i]);
    // if element is already there then insert into duplicate set
    else
      duplicate.insert(arr[i]);
  }

  // printing the result
  cout << "Duplicated items are : ";
  unordered_set<int>::iterator itr;
  for (itr = duplicate.begin(); itr != duplicate.end(); itr++)
    cout << *itr << " ";
  cout << endl;

  // printing the result
  cout << "Initial items are : ";
  for (itr = intSet.begin(); itr != intSet.end(); itr++)
    cout << *itr << " ";
  cout << endl;
}
