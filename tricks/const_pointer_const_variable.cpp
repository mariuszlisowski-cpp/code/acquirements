#include <iostream>

int main() {
    int const i{ 42 };
    const int j{ 24 };

    float k{ 3.14 };
    const float* ptrA = &k;          // pointer to const variable
    // *ptrA = 1.6;                  // read only variable

    float d{ 4.5 };
    float const* ptrB = &d;          // saa
    // *ptrB = 5.5;                  // saa
    ptrB = nullptr;

    float* const ptrC = &d;          // const pointer to variable
    *ptrC = 5.5;
    // ptrC = nullptr;               // read only pointer

    return 0;
}
