// C++ data types

// Type	                Typical Bit Width	Typical Range
//
// char	                   1byte	            -127 to 127 or 0 to 255
// unsigned char	          1byte	            0 to 255
// signed char	             1byte	            -127 to 127
// int	                   4bytes	            -2,147,483,648 to 2,147,483,647
// unsigned int	          4bytes	            0 to 4294967295
// signed int	             4bytes	            -2,147,483,648 to 2,147,483,647
// short int	             2bytes	            -32,768 to 32,767
// unsigned short int	    2bytes	            0 to 65,535
// signed short int	       2bytes	            -32768 to 32767
// long int	                8bytes	            -2,147,483,648 to 2,147,483,647
// signed long int	       8bytes	            same as long int
// unsigned long int	       8bytes	            0 to 4,294,967,295
// long long int	          8bytes	            -(2^63) to (2^63)-1
// unsigned long long int	 8bytes	            0 to 18,446,744,073,709,551,615
// float	                   4 bytes	
// double	                8 bytes	
// long double	            12 bytes	
// wchar_t	                2 or 4 bytes	      1 wide character
//
// The size of variables might be different from those shown in the above table, 
// depending on the compiler and the computer you are using.

#include <iostream>
using namespace std;

int main() {
   cout << "Size of char : " << sizeof(char) << endl;
   cout << "Size of int : " << sizeof(int) << endl;
   cout << "Size of short int : " << sizeof(short int) << endl;
   cout << "Size of long int : " << sizeof(long int) << endl;
   cout << "Size of float : " << sizeof(float) << endl;
   cout << "Size of double : " << sizeof(double) << endl;
   cout << "Size of wchar_t : " << sizeof(wchar_t) << endl;
   
   return 0;
}
