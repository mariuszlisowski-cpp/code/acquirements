#include <array>
#include <iostream>
#include <iterator>
#include <numeric>  // std::iota
#include <unordered_set>
#include <sstream>
#include <vector>

template<typename T>
std::string dumpContainer(const T& container, std::string sep) {
    typename T::const_iterator it;
    std::stringstream sstream;
 
    for (it = container.begin(); it != container.end(); ++it) {
        // insert a seperator char for all but the first element
        if (it != container.begin()) {
            sstream << sep;
        }
         sstream << *it;
    }
 
    return sstream.str();
}

int main() {
    std::vector<int> vec(5);
    std::iota(vec.begin(), vec.end(), 1);
    std::cout << dumpContainer(vec, " ") << std::endl;

    std::array arr{5, 4, 3 , 2, 1};
    std::cout << dumpContainer(arr, ",") << std::endl;

    std::unordered_set<int> nums{5, 4, 3 , 2, 1};
    std::cout << dumpContainer(nums, "-") << std::endl;

    return 0;
}
