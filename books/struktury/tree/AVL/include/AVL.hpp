#pragma once

#include "BST.hpp"
#include "BSTNode.hpp"

class AVL : public BST {
private:
	BSTNode * root;

	int GetHeight(BSTNode *);
	BSTNode * RotateLeft(BSTNode *);
	BSTNode * RotateRight(BSTNode *);
	BSTNode * Insert(BSTNode *, int);
	BSTNode * Remove(BSTNode *, int);
public:
	AVL();

	void Insert(int);
	void Remove(int);
};