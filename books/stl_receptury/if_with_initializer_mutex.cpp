#include <iostream>
#include <mutex>
#include <thread>

std::mutex the_mutex;

void print(int value) {
	// c++17
	if (std::lock_guard<std::mutex> lg{the_mutex}; value % 2) {
		std::cout << "> mutex locked at value " << value << std::endl;
	}
}

int main() {
	std::thread th1([]{
		for (size_t i{0}; i < 10; ++i) {
			print(i);
		}
	});
	
	std::thread th2([]{
		for (size_t i{0}; i < 10; ++i) {
			print(i);
		}
	});

	th1.join();
	th2.join();

	return 0;
}
