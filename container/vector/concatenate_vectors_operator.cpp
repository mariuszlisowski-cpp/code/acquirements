/* add vectors using operator overloading */
#include <iostream>
#include <iterator>
#include <vector>

template <typename T>
std::vector<T>& operator +=(std::vector<T>& lhs, 
                            const std::vector<T>& rhs)
{
    lhs.insert(lhs.end(), rhs.begin(), rhs.end());
    return lhs;
}

int main() {
    std::vector<int> dest{ 1, 2, 3 };
    std::vector<int> src{ 4, 5, 6, 7};

    dest += src;

    std::copy(dest.begin(), dest.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
