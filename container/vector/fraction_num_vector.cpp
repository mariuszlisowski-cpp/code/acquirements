// Fraction of positives, negatives and zeros in a container

#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

void plusMinus(vector<int>);

int main() {
  vector<int> arr { -4, 3, -9, 0, 4, 1, }; // C++11
  plusMinus(arr);

  return 0;
}

void plusMinus(vector<int> arr) {
  int zero = 0, positive = 0, negative = 0;

  vector<int>::const_iterator it;
  for (it = arr.begin(); it != arr.end(); it++) {
    if (*it == 0)
      zero++;
    if (*it > 0)
      positive++;
    if (*it < 0)
      negative++;
  }

  double fractPos = double(positive) / arr.size();
  double FractNeg = double(negative) / arr.size();
  double fractZer = double(zero) / arr.size();

  cout << fixed << setprecision(6) << fractPos << endl
       << FractNeg << endl << fractZer << endl;
}
