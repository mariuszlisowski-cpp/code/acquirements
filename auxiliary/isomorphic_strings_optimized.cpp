// isomorphic strings (optimized)
// O(n) space
// one map, one set, one loop

#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>

bool isIsomorphic(std::string X, std::string Y) {
    if (X.size() != Y.size())
        return false;
    std::unordered_map<char, char> map;
    std::unordered_set<char> set;
    for (int i = 0; i < X.size(); ++i) {
        char x = X[i], y = Y[i];
        if (map.find(x) != map.end()) {
            if (map[x] != y)
                return false;
        } // curly braces necessary here
        else {
            if (set.find(y) != set.end())
                return false;
            map[x] = y;
            set.insert(y);
        }
    }
    return true;
}

int main() {
    // not isomorphic
    std::string X1 = "abca";
    std::string Y1 = "zbcc";

    // not isomorphic
    std::string X2 = "abcz";
    std::string Y2 = "zbcc";

    // isomorphic
    std::string X3 = "abca";
    std::string Y3 = "zbcz";

    if (isIsomorphic(X3, Y3))
        std::cout << "Isomorphic";
    else
        std::cout << "NOT isomorphic";
    std::cout << std:: endl;

    return 0;
}
