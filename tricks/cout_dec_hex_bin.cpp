#include <bitset>
#include <iostream>

int main() {

    unsigned int value{0x80};
    auto result = value << 1;

    std::cout << "dec hex bin" << std::endl;
    
    std::cout << value << '\t'                                                  // dec
              << std::hex << value << '\t'                                      // hex
              << std::bitset<32>(value) << '\n';                                // bin

    std::cout << std::dec << result << '\t'                                     // dec
              << std::hex << result << '\t'                                     // hex
              << std::bitset<32>(result) << '\n';                               // bin

    return 0;
}
