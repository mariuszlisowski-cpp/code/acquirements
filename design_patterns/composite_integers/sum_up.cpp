#include "sum_up.hpp"

#include <algorithm>

int sum_up(const std::vector<ContainsIntegers*> items) {                    // composite example
    auto sum{0};
    std::for_each(items.begin(), items.end(),
                  [&sum](auto item) {
                      sum += item->sum();                                   // sums up SingleVlaue & ManyValues at once
                  });

    return sum;
}
