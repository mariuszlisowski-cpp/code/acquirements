/*
SYS - Systemy pozycyjne

Konwersja liczby naturalnej n na liczbę o podstawie p:
1. Obliczamy resztę z dzielenia liczby n przez liczbę p, reszta to r1.
2. Obliczamy resztę z dzielenia otrzymanego ilorazu przez liczbę p, mamy r2.
3. Wykonujemy krok drugi dopóki otrzymamy iloraz równy 0 i resztę rk.
4. Otrzymane reszty r1,r2,…,rk zapisujemy prawej do lewej w postaci rk…r2r1.
*/

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

string convert(int, int);

int main()
{
  int p, n; // p - podstawa systemu liczb do przekonwertowania, n - liczba

  cout << "Konwersja liczby naturalnej do postawy 11-16..." << endl;
  cout << "Podaj liczbę do konwersji: ";
  cin >> n;
  cout << "Podaj podstawę systemu do konwersji tej liczby: ";
  cin >> p;
  if ((p < 11) || (p > 16))
    cout << "Nieprwidłowa podstawa!" << endl;
  else
    cout << convert(n, p) << endl;

  return 0;
}

/* Konwersja liczby n na liczbę o podstawie p */
string convert(int aN, int aP) {
  int m;
  char mod;
  string s;
  while (aN > 0) { // dopóki iloraz jest niezerowy
    m = aN % aP; // reszta z dzielenia liczby przez podstawę systemu
    aN = aN / aP; // iloraz całkowity
    if (m > 9) {
      switch (m) {
        case 10: mod = 'A'; break;
        case 11: mod = 'B'; break;
        case 12: mod = 'C'; break;
        case 13: mod = 'D'; break;
        case 14: mod = 'E'; break;
        case 15: mod = 'F'; break;
      }
      s += mod; // zapisywanie reszt w 'string'
    }
    else
      s += to_string(m); // zamiana cyfry (1-9) na typ 'string'
  }
  reverse(s.begin(), s.end()); // odwrócenie ciągu
  return s;
}
