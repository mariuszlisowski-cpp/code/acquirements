#include <iostream>

int findArea(int length, int width); // prototyp funkcji

int main()
{
  int length;
  int width;
  int area;

  std::cout << "\nJak szerokie jest twoje podwórko w metrach? ";
  std::cin >> width;
  std::cout << "Jak długie jest twoje podwórko w metrach? ";
  std::cin >> length;

  area = findArea(length, width);

  std::cout << "\nTwoje podwórko ma " << area << " metrów kwadratowych.\n\n";
  return 0;
}

int findArea(int l, int w) // definicja funkcji
{
  return l * w;
}
