#include "customer.hpp"

Customer::Customer() {}
Customer::Customer(std::string forename, std::string surname)
    : forename_(forename), surname_(surname) {}
Customer::~Customer() = default;

bool Customer::isPersistable() const noexcept {
    return (!forename_.empty() && !surname_.empty());
}

std::string Customer::getForename() const {
    return forename_;
}
std::string Customer::getSurname() const {
    return surname_;
}

bool NotFoundCustomer::isPersistable() const noexcept {
    return false;
}
