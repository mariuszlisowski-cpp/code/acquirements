#include <iostream>
#include <vector>
#include <boost/type_index.hpp>

class Widget {};

std::vector<Widget> createVec() {
    return { Widget(), Widget() };
}

template<typename T>
void func(const T& param) {
    std::cout << typeid(T).name() << std::endl;                                     // PK pointer to const
    std::cout << typeid(param).name() << std::endl;                                 // 6 chars in class name
}

template<typename T>
void func_boost(const T& param) {
    using boost::typeindex::type_id_with_cvr;
    std::cout << type_id_with_cvr<T>().pretty_name() << std::endl;                  // Widget const*
    std::cout << type_id_with_cvr<decltype(param)>().pretty_name() << std::endl;    // Widget const* const&
}

int main() {
    const auto v = createVec();

    if (!v.empty()) {
        func(&v[0]);                                                                // argument is an address
        func_boost(&v[0]);                                                          // thus pointer ia a paramater

    }

    return 0;
}
