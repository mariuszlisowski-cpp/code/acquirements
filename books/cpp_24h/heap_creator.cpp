#include <iostream>

using namespace std;

class SimpleCat
{
public:
  SimpleCat();
  ~SimpleCat();
private:
  int itsAge;
};

SimpleCat::SimpleCat()
{
  cout << "\tWywołano kontstuktor.\n";
  itsAge = 1;
}

SimpleCat::~SimpleCat()
{
  cout << "\tWywołano destruktor.\n";
}

int main()
{
  cout << "\nSimpleCat Frisky...\n";
  SimpleCat Frisky;

  cout << "SimpleCat *pRags = new SimpleCat...\n";
  SimpleCat *pRags = new SimpleCat;

  cout << "delete pRags...\n";
  delete pRags;

  cout << "Wyjście, czekaj na Frisky...\n";

  return 0;
}
