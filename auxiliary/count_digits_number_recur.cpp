#include <iostream>

int countDigitsInRecur(int number) {
    static int counter{};
    if (number > 0) {
        ++counter;
        countDigitsInRecur(number /= 10); 
    }
    
    return counter;
}

int countDigitsIn(int number) {
    int counter{};
    do {
        ++counter;

    } while (number /= 10);

    return counter;
}

int main() {
    std::cout << countDigitsIn(874) << std::endl;
    std::cout << countDigitsInRecur(8555) << std::endl;

    return 0;
}
