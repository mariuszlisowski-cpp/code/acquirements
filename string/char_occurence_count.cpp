/*
Zliczanie wystąpień znaku
*/

#include <iostream>
#include <algorithm> // funkcja 'counts'

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::count;

int main()
{
  int n;
  string s;
  cout << "Wpisz jakieś zdanie..." << endl << ": ";
  getline(cin, s);
  cout << "Znaleziono ";

  n =  count(s.begin(), s.end(), ' '); // wyszukuje wszystkie znaki 'space'

  if (n == 1)
    cout << n << " znak ";
  else
    if (n == 2 || n == 3 || n == 4)
      cout << n << " znaki ";
    else
      cout << n << " znaków ";
  cout << "space" << endl;
  return 0;
}
