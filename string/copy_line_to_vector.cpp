/*
Get line and copy to vector
~ pobiera linię liczb do łańcucha i zamienia na kontener liczb typu 'int'
*/

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <sstream>

int main() {
  std::string line;
  if(!std::getline(std::cin, line)) return 1;
  std::istringstream iss(line);

  std::vector<int> vec;
  std::copy(std::istream_iterator<int>(iss), std::istream_iterator<int>(), std::back_inserter(vec));

  std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));

  std::cout << std::endl;
  return 0;
}
