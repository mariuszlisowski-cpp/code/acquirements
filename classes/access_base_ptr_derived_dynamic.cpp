/* access to derived method through pointer to base */
#include <iostream>

class IBase {
public:
    virtual ~IBase() {}                                 // polimorphic class
    
    virtual void f() = 0;
    virtual void g() = 0;
};

class Derived : public IBase {
public:
    void f() override {}
    void g() override {}
    void h() {
        std::cout << "h called" << std::endl;
    }
};

int main() {
    IBase* base{};                                      // polimorphic pointer

    base = new Derived;

    auto derived = dynamic_cast<Derived*>(base);        // thus dynamic_cast
    if (derived) {
        derived->h();
    }

    dynamic_cast<Derived*>(base)->h();                  // directly but dangerous

    return 0;
}
