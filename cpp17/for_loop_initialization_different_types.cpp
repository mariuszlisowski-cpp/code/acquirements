#include <cstring>
#include <iostream>
#include <tuple>

// c++11
void for_loop_with_variabled_of_different_types_pair_11(const char* cstr){
    for (auto pair{ std::make_pair(0, cstr[0]) }; pair.second != '\0'; ++pair.first) {
        std::cout << pair.second;
        pair.second = cstr[pair.first + 1];
    }
}

// c++17
void for_loop_with_variabled_of_different_types_pair_17(const char* cstr){
    for (auto [i, ch] { std::make_pair(0, cstr[0]) }; ch != '\0'; ++i) {
        std::cout << ch;
        ch = cstr[i + 1];
    }
}

// c++11
void for_loop_with_variabled_of_different_types_tuple_11(const char* cstr){
    for (auto tuple {std::make_tuple(0, cstr[0], std::strlen(cstr))};
         std::get<1>(tuple) != '\0';
         ++std::get<0>(tuple))
    {
        std::cout << std::get<1>(tuple);                        // cstr[]
        std::get<1>(tuple) = cstr[std::get<0>(tuple) + 1];
        std::get<2>(tuple);                                     // length of cstr (not used here)
    }
}

// c++17
void for_loop_with_variabled_of_different_types_tuple_17(const char* cstr){
    for (auto [i, ch, len] { std::make_tuple(0, cstr[0], std::strlen(cstr)) }; ch != '\0'; ++i) {
        std::cout << ch;
        ch = cstr[i + 1];
        --len;                                                  // length of cstr (not used here)
    }
}

void while_equivalent(const char* cstr) {
    size_t i{};
    char c{ cstr[0] };
    while (c != '\0') {
        std::cout << c;
        c = cstr[++i];
    }
}

int main() {
    std::strlen("dd");
    const char* cstring{ "abcd_" };

    for_loop_with_variabled_of_different_types_pair_11(cstring);
    for_loop_with_variabled_of_different_types_pair_17(cstring);
    for_loop_with_variabled_of_different_types_tuple_11(cstring);
    for_loop_with_variabled_of_different_types_tuple_17(cstring);
    while_equivalent(cstring);

    return 0;
}
