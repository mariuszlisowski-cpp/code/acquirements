#include <iostream>

/* 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 seq
   0  1  2  3  4  5  6   7   8   9  10  11 th */
int getNthFib_from_zero(int n) {
    if (n == 0) {
        return 0; // first base case (2 - 2 = 0)
    }
    if (n == 1) {
        return 1; // second base case (2 - 1 = 1)
    }

    return getNthFib_from_zero(n - 1) + getNthFib_from_zero(n - 2); // last calls for n{2-1} and n{2-2} thus two cases
}

/* 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 seq
   1  2  3  4  5  6  7   8   9  10  11  12 th */
int getNthFib_from_one(int n) {
    if (n == 1) {
        return 0;
    }
    if (n == 2) {
        return 1;
    }

    return getNthFib_from_one(n - 1) + getNthFib_from_one(n - 2);
}



int main() {
    const int nth_fibonacci = 8;

    std::cout << nth_fibonacci << "th Fibonacci number (counting from zero): " 
              << getNthFib_from_zero(nth_fibonacci) << std::endl;
    std::cout << nth_fibonacci << "th Fibonacci number (counting from one):  " 
              << getNthFib_from_one(nth_fibonacci) << std::endl;

    return 0;
}

