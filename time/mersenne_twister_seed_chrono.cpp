#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

int main() {
    constexpr size_t CONTAINER_SIZE{ 10 };
    constexpr int INITIAL_VALUE{ 1 };

    std::vector<int> v(CONTAINER_SIZE);
    std::iota(begin(v), end(v), INITIAL_VALUE);

    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(begin(v), end(v),
                 std::mt19937(seed));

    for(auto& el : v) {
        std::cout << el << ' ';
    }

    return 0;
}
