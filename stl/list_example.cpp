// STL doubly linked list example
// #push_front #push_back #insert #erase #find

#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

// typedef typename list<T>::iterator listIterator;

template<class T>
void showList(const list<T> &lst) {
    for (auto x : lst)
        cout << x << " -> ";
    cout << "NULL" << endl;
}

// not random access iterator (need to increment an index)
template<class T>
typename list<T>::iterator getIterator(typename list<T>::iterator it, T index) {
    for (auto i = 0; i < index; ++i, ++it);
    return it;
}

int main() {
    list<int> linkedList;
    list<int>::iterator it;

    // head & tail
    linkedList.push_front(20);
    linkedList.push_back(80);

    // insert at head
    linkedList.insert(linkedList.begin(), 10);

    // insert at tail
    linkedList.insert(linkedList.end(), 90);

    // insert at index x
    it = getIterator(linkedList.begin(), 2);
    linkedList.insert(it, 30);

    // display
    showList(linkedList);

    // get a value
    int index = 4;
    it = getIterator(linkedList.begin(), 4);
    cout << *it << " at position " << index << endl;

    // find a value
    int val = 30;
    it = find(linkedList.begin(), linkedList.end(), val);
    index = distance(linkedList.begin(), it);
    if (it != linkedList.end())
        cout << val << " found at position " << index << endl;
    else
        cout << val << " not found!" << endl;

    // erase
    linkedList.erase(linkedList.begin());
    it = getIterator(linkedList.begin(), 3);
    linkedList.erase(it);

    // display
    showList(linkedList);

    return 0;
}
