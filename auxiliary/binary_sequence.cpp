// Binary sequence
// ~ finds the longest sequence of 0's in binary representation of an integer

#include <iostream>

using std::cout;
using std::endl;
using std::string;

class Solution {
  public:
    int solution(int N);
};

int Solution::solution(int N) {
    string binary;
    while (N != 0) {
        binary = (N % 2 == 0 ? "0" : "1") + binary;
        N /= 2;
    }

    int max = 0, count = 0;
    for (int i = 0; i < binary.size(); i++) {
        if (binary[i] == '1') {
            if (count > max) {
                max = count;
            }
            count = 0;
        } else {
            count++;
            // if the last sequence is the longest
            if (i == binary.size() - 1) {
                if (count > max) {
                    max = count;
                }
            }
        }
    }
    return max;
}

int main() {
    Solution bin;
    cout << bin.solution(20) << endl;

    return 0;
}
