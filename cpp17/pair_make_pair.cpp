#include <iostream>
#include <string>
#include <utility>

int main() {
    std::pair<std::string, std::string> string_pair("hi", "there");
    std::cout << string_pair.first <<  " : " << string_pair.second << std::endl;

    std::pair deduced_pair('a', 1); // C++ 17
    std::cout << deduced_pair.first <<  " : " << deduced_pair.second << std::endl;

    auto char_pair{ std::make_pair('!', '?')};
    std::cout << char_pair.first << " : " << char_pair.second << std::endl;

    return 0;
}
