#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template <typename R, typename ... Args>
auto matches_count(const R& range, Args&& ... args) {
    return (std::count(begin(range), end(range), args) + ...);
    /* the above unfolds to:
    return (std::count(begin(range), end(range), 1) +           // no match (0)
           (std::count(begin(range), end(range), 9) +           // match (1) 
           (std::count(begin(range), end(range), 7));           // match (1)
    */                                                          // returns added up matches
}

int main() {
    std::vector<int> v{ 7, 8, 9 };

    auto result{ matches_count(v, 1, 9, 7) };                   // 9, 7 -> 2 matches (int)
    std::cout << result << std::endl;

    std::string str{ "abcdefgh" };                              // must not be cstring
    result = matches_count(str, 'z', 'f', 'g', 'h');            // f, g ,h -> 3 matches
    std::cout << result << std::endl;
}
