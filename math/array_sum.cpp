#include <iostream>
using namespace std;

int main()
{
    int arr[] {8,6,2,6};
    size_t size = sizeof(arr) / sizeof(*arr);
    int sum{};
    for (size_t i = 0; i < size; ++i) {
        sum += *(arr + i);
    }
    cout << "Suma wartosci wlementow z tablicy wynosi: " << sum;

}