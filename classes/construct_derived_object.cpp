#include <iostream>

class Animal
{
public:
    Animal() {                                                              // calles 1st
        std::cout << typeid(this).name() << std::endl;
    }
};

class Human : public Animal
{
public:
    Human() {                                                               // called 2nd
        std::cout << typeid(this).name() << std::endl;
    }
};

int main() {
    Human human{};                                                          // base class c'tor then derived class one

    return 0;
}
