// Queue class header

#include "queue_element.h"

class Queue {
public:
  QueueElement* first = NULL;
  QueueElement* last = NULL;

  // queue methods
  void enqueueCharacter(char);
  char dequeueCharacter();
  void showQueue();
};
