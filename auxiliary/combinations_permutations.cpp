// combinations & permutations

#include <iostream>

constexpr unsigned long long factorial(int n) {
    return n == 0 ? 1 : n * factorial(n - 1);
}

long permutations(int m, int n) {
    if (m < n) {
        m ^= n ^= m ^= n; // swap
    }
    long permutations = factorial(m) / factorial(m - n);

    return permutations;
}

long combinationsA(int m, int n) {
    long combinations = permutations(m, n) / factorial(n);
    
    return combinations;
}

long combinationsB(int m, int n) {
    if (m < n) {
        m ^= n ^= m ^= n; // swap
    }
    long combinations = factorial(m) / (factorial(m - n) * factorial(n));

    return combinations;
}

int main() {
    std::cout << "Permutations: " << permutations(3, 5) << std::endl; // 
    std::cout << "Combinations: " << combinationsA(5, 3) << std::endl; // 
    std::cout << "Combinations: " << combinationsB(3, 5) << std::endl; // 


    return 0;
}
